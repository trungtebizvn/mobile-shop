<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<s:include value="templates/head.jsp" />
<style>
.complete-page.margin-top-20.float-left {
	text-align: center;
}

.customer-notes {
	margin: 50px 0px;
}

.buttons-container {
	margin-top: 10px;
	float: right;
}

.checkout-shipping-block--title {
	font-size: 25px;
	font-weight: 600;
	color: green;
	border-bottom: 1px dotted #e4e4e4;
	margin-bottom: 20px;
	text-transform: uppercase;
}

.txt_thank_desc {
	font-size: 18px;
	font-weight: 600;
	margin-bottom: 5px;
	text-transform: uppercase;
}
</style>
</head>
<body>

	<div class="wrapper">
		<section id="checkout-page">
			<div class="container">

				<div class="complete-page margin-top-20 float-left">
					<div class="customer-notes" style="padding-top: 0;">
						<div class="checkout-shipping-block--title"
							style="padding-top: 0;color:red">NOT FOUND</div>
						<!-- <div class="txt_thank_desc">Your order has been successfully
							processed</div> -->
						<div class="txt_thank_desc_2">
							Please, contact for us when you see this error many time.<br>
							Thank you !
							
						</div>
						
					</div>
				</div>

			</div>
			<!-- /.container -->
		</section>
		<!-- /#checkout-page -->
	</div>
	<!-- FOOTER -->
	<s:include value="templates/footer.jsp" />
</body>
</html>