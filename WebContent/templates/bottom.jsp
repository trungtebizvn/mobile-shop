<%@ taglib prefix="s" uri="/struts-tags"%>
<section id="recently-reviewd" class="wow fadeInUp">
	<div class="container">
		<div class="carousel-holder hover">

			<div class="title-nav">
				<h2 class="h1">Most Viewed</h2>
				<div class="nav-holder">
					<a href="#prev" data-target="#owl-recently-viewed"
						class="slider-prev btn-prev fa fa-angle-left"></a> <a href="#next"
						data-target="#owl-recently-viewed"
						class="slider-next btn-next fa fa-angle-right"></a>
				</div>
			</div>
			<!-- /.title-nav -->

			<div id="owl-recently-viewed"
				class="owl-carousel product-grid-holder">
				
				<s:iterator value="mostViewedProducts" var="product">
				<div class="no-margin carousel-item product-item-holder size-small hover">					
					<div class="product-item">
						<div class="image">
							<img alt="logo" src="<s:property value="img"/>" width="100%" height="20%"/>
						</div>
						<div class="body">
							<div class="title">
								<s:url action="show" var="url">
									<s:param name="pid">
										<s:property value="id" />
									</s:param>
								</s:url>
								<a href="<s:property value="#url"/>"><s:property value="title" /></a>
							</div>
							<div class="brand"><s:property value="brand" /></div>
						</div>
						<div class="prices">
							<div class="price-current text-right">$<s:property value="getText('{0,number,#,###.00}',{price})" /></div>
						</div>
						<div class="hover-area">
							<div class="add-cart-button">
								<s:url action="show" var="url">
									<s:param name="pid">
										<s:property value="id" />
									</s:param>
								</s:url>
								<a href="<s:property value="#url"/>" class="le-button">add
														to cart</a>
							</div>
						</div>
					</div>
					<!-- /.product-item -->
				</div>
				<!-- /.product-item-holder -->
				</s:iterator>
				
			</div>
			<!-- /#recently-carousel -->
		</div>
		<!-- /.carousel-holder -->
	</div>
	<!-- /.container -->
</section>
<!-- /#most-reviewd -->