<%@ taglib prefix="s" uri="/struts-tags"%>
<!-- NAVIGATION -->
<nav id="top-megamenu-nav" class="megamenu-vertical animate-dropdown">
	<div class="container">
		<div class="yamm navbar">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#mc-horizontal-menu-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
			</div>
			<!-- /.navbar-header -->
			<div class="collapse navbar-collapse"
				id="mc-horizontal-menu-collapse">
				<ul class="nav navbar-nav">
					
					
					<s:url action="browse" var="Nokia">
					<s:param name="brand">Nokia</s:param>
					</s:url>
					<li class="dropdown"><a href="<s:property value="Nokia"/>"
						class="dropdown-toggle">Nokia</a></li>

					<s:url action="browse" var="Samsung">
					<s:param name="brand">Samsung</s:param>
					</s:url>
					<li class="dropdown"><a href="<s:property value="Samsung"/>"
						class="dropdown-toggle">Samsung</a></li>
					
					<s:url action="browse" var="HTC">
					<s:param name="brand">HTC</s:param>
					</s:url>
					<li class="dropdown"><a href="<s:property value="HTC"/>"
						class="dropdown-toggle">HTC</a></li>
					
					<s:url action="browse" var="BlackBerry">
					<s:param name="brand">BlackBerry</s:param>
					</s:url>
					<li class="dropdown"><a href="<s:property value="BlackBerry"/>" class="dropdown-toggle">BlackBerry</a></li>
					
					<s:url action="browse" var="LG">
					<s:param name="brand">LG</s:param>
					</s:url>
					<li class="dropdown"><a href="<s:property value="LG"/>" class="dropdown-toggle">LG</a></li>

					<s:url action="browse" var="Apple">
					<s:param name="brand">Apple</s:param>
					</s:url>
					<li class="dropdown"><a href="<s:property value="Apple"/>" class="dropdown-toggle">IPhone</a></li>


					<s:url action="browse" var="Sony">
					<s:param name="brand">Sony</s:param>
					</s:url>
					<li class="dropdown"><a href="<s:property value="Sony"/>" class="dropdown-toggle">Sony</a></li>
				</ul>
				<!-- /.navbar-nav -->
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.navbar -->
	</div>
	<!-- /.container -->
</nav>