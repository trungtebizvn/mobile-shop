<%@ taglib prefix="s" uri="/struts-tags"%>
<nav class="top-bar animate-dropdown">
	<div class="container">
		<div class="col-xs-12 col-sm-6 no-margin">
			<ul>
				<s:url action="index" var="indexUrl" />
				<li><s:a href="%{indexUrl}">Home</s:a></li>
				<li><a href="#">FAQ</a></li>
				<li><a href="#">Contact</a></li>
			</ul>
		</div>
		<!-- /.col -->

		<%-- <div class="col-xs-12 col-sm-6 no-margin">
			<ul class="right">
				<li>
					<s:a href="authentication.jsp">Register</s:a>
					</li>
				<li>
					<s:url action="man-product-show" var="productUrl" />
					<s:a href="%{productUrl}">Login</s:a>
				</li>
			</ul>
		</div> --%>
		<!-- /.col -->
	</div>
	<!-- /.container -->
</nav>
<!-- /.top-bar -->
<header>
	<div class="container no-padding">
		<div class="col-xs-12 col-sm-12 col-md-3 logo-holder">
			<!-- LOGO -->
			<div class="logo">
				<h3>Mobile</h3>
				<p>OPEN A NEW WORLD</p>
			</div>
			<!-- /.logo -->
		</div>
		<!-- /.logo-holder -->

		<div class="col-xs-12 col-sm-12 col-md-6 top-search-holder no-margin">
			<div class="contact-row">
				<div class="phone inline">
					<i class="fa fa-phone"></i> (+84) 123 456 7890
				</div>
				<div class="contact inline">
					<i class="fa fa-envelope"></i> contact@<span class="le-color">oursupport.com</span>
				</div>
			</div>
			<!-- /.contact-row -->
			<!-- SEARCH AREA -->
			<div class="search-area">
				<form>
					<div class="control-group">
						<input class="search-field" placeholder="Search for item" /> <a
							class="search-button" href="#"></a>

					</div>
				</form>
			</div>
			<!-- /.search-area -->
			<!-- SEARCH AREA -->
		</div>
		<!-- /.top-search-holder -->

		<div class="col-xs-12 col-sm-12 col-md-3 top-cart-row no-margin">
			<div class="top-cart-row-container">
				<div class="wishlist-compare-holder">
					<!-- <div class="wishlist ">
			            <a href="#"><i class="fa fa-heart"></i> wishlist <span class="value">(21)</span> </a>
			        </div>
			        <div class="compare">
			            <a href="#"><i class="fa fa-exchange"></i> compare <span class="value">(2)</span> </a>
			        </div> -->
				</div>

				<!-- SHOPPING CART DROPDOWN  -->
				<div class="col-xs-12 col-sm-12 col-md-12 top-cart-row no-margin">
					<div class="top-cart-row-container">
						<div class="wishlist-compare-holder">
							<!-- <div class="wishlist ">
			            <a href="#"><i class="fa fa-heart"></i> wishlist <span class="value">(21)</span> </a>
			        </div>
			        <div class="compare">
			            <a href="#"><i class="fa fa-exchange"></i> compare <span class="value">(2)</span> </a>
			        </div> -->
						</div>

						<!-- SHOPPING CART DROPDOWN  -->
						<div class="top-cart-holder dropdown animate-dropdown">
							<div class="basket">

								<a class="dropdown-toggle" data-toggle="dropdown" href="#">
									<div class="basket-item-count">
										<span class="count"><s:property value="amount" /></span> <img
											src="assets/images/icon-cart.png" alt="" />
									</div>

									<div class="total-price-basket">
										<span class="lbl">your cart:</span> <span class="total-price">
											<span class="sign">$</span><span class="value">
												<s:property value="getText('{0,number,#,###.00}',{total})" />
											</span>
										</span>
									</div>
								</a>

								<ul class="dropdown-menu">
									<s:iterator value="products" var="product">
										<li>
											<div class="basket-item">
												<div class="row">
													<div class="col-xs-4 col-sm-4 no-margin text-center">
														<div class="thumb">
															<a href="#" class="thumb-holder"> 
															<img class="lazy" height="73" width="73" alt="item logo"
																src="<s:property value="key.img"/>" />
															</a>
														</div>
													</div>
													<div class="col-xs-8 col-sm-8 no-margin">
														<div class="title">
															<s:property value="value " /> x <strong><s:property value="key.title" /></strong>
														</div>
														<div class="price">
															<s:set var="totalItem" value="%{value * key.price}" />
															$
															<s:property value="getText('{0,number,#,###.00}',{#attr.totalItem})" />
															
														</div>
													</div>
												</div>
												<s:url action="removeFromCart" var="urlRemove">
													<s:param name="pid">
														<s:property value="key.id" />
													</s:param>
												</s:url>
												<a class="close-btn" href="<s:property value="#urlRemove"/>"></a>
											</div>
										</li>
									</s:iterator>

									<li class="checkout">
										<div class="basket-item">
											<div class="row">
												<s:set var="disabled"></s:set>
												
												<s:url action="viewCart" var="cartUrl" />
												<s:url action="checkout" var="checkoutUrl" />
												<div class="col-xs-12 col-sm-6">
													<s:if test="amount == 0">
														<a class="le-button inverse" href="#">View cart</a>
													</s:if>
													<s:else>
														<a class="le-button inverse" href="<s:property value="cartUrl"/>">View cart</a>
													</s:else>
																
												</div>
												<div class="col-xs-12 col-sm-6">
													<s:if test="amount == 0">
														<a class="le-button button" href="#">checkout</a>
													</s:if>
													<s:else>
														<a class="le-button button" href="<s:property value="checkoutUrl"/>">checkout</a>
													</s:else>
													
												</div>
											</div>
										</div>
									</li>

								</ul>
							</div>
							<!-- /.basket -->
						</div>
						<!-- /.top-cart-holder -->
					</div>
					<!-- /.top-cart-row-container -->
				</div>
				<!-- /.top-cart-row -->

			</div>
			<!-- /.container -->
		</div>
</header>