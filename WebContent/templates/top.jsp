<%@ taglib prefix="s" uri="/struts-tags"%>
<div id="top-banner-and-menu">
			<div class="container">
				<s:include value="sidebar-navigation.jsp" />
				<div class="col-xs-12 col-sm-8 col-md-9 homebanner-holder">
					<div id="hero">
						<div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">

							<div class="item"
								style="background-image: url(assets/images/sliders/slider01.jpg);">
							</div>
							<!-- /.item -->

							<div class="item"
								style="background-image: url(assets/images/sliders/slider02.jpg);">
							</div>
							<!-- /.item -->

						</div>
						<!-- /.owl-carousel -->
					</div>
				</div>
				<!-- /.homebanner-holder -->

			</div>
			<!-- /.container -->
		</div>
		<!-- /#top-banner-and-menu -->