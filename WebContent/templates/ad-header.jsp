<%@ taglib prefix="s" uri="/struts-tags"%>
<!-- TOP NAVIGATION : END -->
<!-- HEADER  -->
<header class="no-padding-bottom header-alt">
	<div class="container no-padding">

		<div class="col-xs-12 col-md-3 logo-holder">
			<!--  LOGO -->
			<div class="logo">
				<s:url action="index" var="indexUrl" />
				<h3>Mobile-Administrator</h3>
				<p><s:a href="%{indexUrl}">Center - Supermarket</s:a></p>
			</div>
			<!-- /.logo -->
		</div>
		<!-- /.logo-holder -->

	</div>
	<!-- /.container -->
	<!-- ========================================= NAVIGATION ========================================= -->
	<nav id="top-megamenu-nav" class="megamenu-vertical animate-dropdown">
		<div class="container">
			<div class="yamm navbar">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target="#mc-horizontal-menu-collapse">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
				</div>
				<!-- /.navbar-header -->
				<div class="collapse navbar-collapse"
					id="mc-horizontal-menu-collapse">
					<ul class="nav navbar-nav">
						<s:url action="man-product-show" var="productUrl" />
						
						<li class="dropdown"><s:a href="%{productUrl}">Product</s:a></li>
						<s:url action="man-invoice-show" var="invoiceUrl" />
						<li class="dropdown"><s:a href="%{invoiceUrl}">Invoice</s:a></li>

					</ul>
					<!-- /.navbar-nav -->
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.navbar -->
		</div>
		<!-- /.container -->
	</nav>
	<!-- /.megamenu-vertical -->
</header>