<%@ taglib prefix="s" uri="/struts-tags"%>
<!--TOP NAVIGATION -->
<div class="col-xs-12 col-sm-4 col-md-3 sidemenu-holder">
	<div class="side-menu animate-dropdown">
		<div class="head">
			<i class="fa fa-list"></i> all departments
		</div>
		<nav class="yamm megamenu-horizontal" role="navigation">
			<ul class="nav">
				<s:url action="browse" var="Nokia">
					<s:param name="brand">Nokia</s:param>
				</s:url>
				<li class="dropdown menu-item">
					<a href="<s:property value="Nokia"/>">Nokia</a>
				</li>
				
				<s:url action="browse" var="Samsung">
					<s:param name="brand">Samsung</s:param>
				</s:url>
				<li class="dropdown menu-item"><a href="<s:property value="Samsung"/>">Samsung</a>
				</li>
				<!-- /.menu-item -->

				<s:url action="browse" var="HTC">
					<s:param name="brand">HTC</s:param>
				</s:url>
				<li class="dropdown menu-item"><a href="<s:property value="HTC"/>">HTC</a>
				</li>
				<!-- /.menu-item -->

				<s:url action="browse" var="Blackberry">
					<s:param name="brand">Blackberry</s:param>
				</s:url>
				<li class="dropdown menu-item"><a href="<s:property value="Blackberry"/>">Blackberry</a></li>
				<!-- /.menu-item -->

				<s:url action="browse" var="LG">
					<s:param name="brand">LG</s:param>
				</s:url>
				<li class="dropdown menu-item"><a href="<s:property value="LG"/>">LG</a></li>
				<!-- /.menu-item -->


				<s:url action="browse" var="Apple">
					<s:param name="brand">Apple</s:param>
				</s:url>
				<li class="dropdown menu-item"><a href="<s:property value="Apple"/>">Iphone</a></li>
				<!-- /.menu-item -->


				<s:url action="browse" var="Sony">
					<s:param name="brand">Sony</s:param>
				</s:url>
				<li class="dropdown menu-item"><a href="<s:property value="Sony"/>">Sony</a></li>
				<!-- /.menu-item -->

			</ul>
			<!-- /.nav -->
		</nav>
		<!-- /.megamenu-horizontal -->
	</div>
	<!-- /.side-menu -->
</div>
<!-- /.sidemenu-holder -->