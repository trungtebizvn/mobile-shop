<%@ taglib prefix="s" uri="/struts-tags"%>
<footer id="footer" class="color-bg">
	<div class="link-list-row">
		<div class="container no-padding">
			<div class="col-xs-12 col-md-4 ">
				<!-- ============================================================= CONTACT INFO ============================================================= -->
				<div class="contact-info">
					<div class="footer-logo" style='color: green'>
						<h1>Mobile</h1>
						<p>OPEN A NEW WORLD</p>
					</div>
					<!-- /.footer-logo -->
					<p>1-2 D3 street, Ward 25 , Binh Thanh district, Ho Chi Minh
						city.</p>
					<div class="social-icons">
						<h3>Get in touch</h3>
						<ul>
							<li><a href="http://facebook.com/transvelo"
								class="fa fa-facebook"></a></li>
							<li><a href="#" class="fa fa-twitter"></a></li>
							<li><a href="#" class="fa fa-pinterest"></a></li>
							<li><a href="#" class="fa fa-linkedin"></a></li>
							<li><a href="#" class="fa fa-stumbleupon"></a></li>
							<li><a href="#" class="fa fa-dribbble"></a></li>
							<li><a href="#" class="fa fa-vk"></a></li>
						</ul>
					</div>
					<!-- /.social-icons -->
				</div>
				<!-- ============================================================= CONTACT INFO : END ============================================================= -->
			</div>

			<div class="col-xs-12 col-md-8 no-margin">
				<!-- ============================================================= LINKS FOOTER ============================================================= -->
				<div class="link-widget">
					<div class="widget">
						<h3>Find it fast</h3>
						<ul>


							<s:url action="browse" var="Nokia">
								<s:param name="brand">Nokia</s:param>
							</s:url>
							<li><a href="<s:property value="Nokia"/>"
								>Nokia</a></li>

							<s:url action="browse" var="Samsung">
								<s:param name="brand">Samsung</s:param>
							</s:url>
							<li><a href="<s:property value="Samsung"/>"
								>Samsung</a></li>

							<s:url action="browse" var="HTC">
								<s:param name="brand">HTC</s:param>
							</s:url>
							<li><a href="<s:property value="HTC"/>"
								>HTC</a></li>

							<s:url action="browse" var="BlackBerry">
								<s:param name="brand">BlackBerry</s:param>
							</s:url>
							<li><a
								href="<s:property value="BlackBerry"/>" >BlackBerry</a></li>

							<s:url action="browse" var="LG">
								<s:param name="brand">LG</s:param>
							</s:url>
							<li><a href="<s:property value="LG"/>"
								>LG</a></li>

							<s:url action="browse" var="Apple">
								<s:param name="brand">Apple</s:param>
							</s:url>
							<li><a href="<s:property value="Apple"/>"
								>IPhone</a></li>


							<s:url action="browse" var="Sony">
								<s:param name="brand">Sony</s:param>
							</s:url>
							<li><a href="<s:property value="Sony"/>"
								>Sony</a></li>
						</ul>
					</div>
					<!-- /.widget -->
				</div>
				<!-- /.link-widget -->

				<div class="link-widget">
					<div class="widget">
						<h3>Information</h3>
						<ul>
							<li><a href="category-grid.html">Find a Store</a></li>
							<li><a href="category-grid.html">About Us</a></li>
							<li><a href="category-grid.html">Contact Us</a></li>
						</ul>
					</div>
					<!-- /.widget -->
				</div>
				<!-- /.link-widget -->

				<div class="link-widget">
					<div class="widget">
						<h3>Information</h3>
						<ul>
							<li><a href="category-grid.html">Customer Service</a></li>
							<li><a href="category-grid.html">FAQs</a></li>
							<li><a href="category-grid.html">Product Support</a></li>
						</ul>
					</div>
					<!-- /.widget -->
				</div>
				<!-- /.link-widget -->
				<!-- ============================================================= LINKS FOOTER : END ============================================================= -->
			</div>
		</div>
		<!-- /.container -->
	</div>
	<!-- /.link-list-row -->

	<div class="copyright-bar">
		<div class="container">
			<div class="col-xs-12 col-sm-6 no-margin">
				<div class="copyright">
					&copy; <a href="index.html">Supermarket-Mobile</a> - all rights
					reserved
				</div>
				<!-- /.copyright -->
			</div>
			<div class="col-xs-12 col-sm-6 no-margin">
				<div class="payment-methods ">
					<ul>
						<li><img alt="" src="assets/images/payments/payment-visa.png"></li>
						<li><img alt=""
							src="assets/images/payments/payment-master.png"></li>
						<li><img alt=""
							src="assets/images/payments/payment-paypal.png"></li>
						<li><img alt=""
							src="assets/images/payments/payment-skrill.png"></li>
					</ul>
				</div>
				<!-- /.payment-methods -->
			</div>
		</div>
		<!-- /.container -->
	</div>
	<!-- /.copyright-bar -->
</footer>
<!-- /#footer -->

<!-- JavaScripts placed at the end of the document so the pages load faster -->
<script src="assets/js/jquery-1.10.2.min.js"></script>
<script src="assets/js/jquery-migrate-1.2.1.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script
	src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
<script src="assets/js/gmap3.min.js"></script>
<script src="assets/js/bootstrap-hover-dropdown.min.js"></script>
<script src="assets/js/owl.carousel.min.js"></script>
<script src="assets/js/css_browser_selector.min.js"></script>
<script src="assets/js/echo.min.js"></script>
<script src="assets/js/jquery.easing-1.3.min.js"></script>
<script src="assets/js/bootstrap-slider.min.js"></script>
<script src="assets/js/jquery.raty.min.js"></script>
<script src="assets/js/jquery.prettyPhoto.min.js"></script>
<script src="assets/js/jquery.customSelect.min.js"></script>
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/scripts.js"></script>