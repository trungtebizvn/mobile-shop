<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<s:include value="templates/head.jsp" />
</head>
<body>

	<div class="wrapper">
		<!--  HEADER  -->
		<s:action name="header" executeResult="true">
		</s:action>
		<s:include value="templates/main-navigation.jsp" />
		<!-- <div id="breadcrumb-alt">
			<div class="container">
				<div class="breadcrumb-nav-holder minimal">
					<ul>
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item current"><a href="#">shopping
								cart</a></li>
					</ul>
				</div>
				.breadcrumb-nav-holder
			</div>
			/.container
		</div>
		/#breadcrumb-alt -->
	</div>

	<section id="category-grid">
	<div class="container">

		<!-- SIDEBAR -->
		<div class="col-xs-12 col-sm-3 no-margin sidebar narrow">
			<div class="widget">
				<h1 class="border">special offers</h1>
				<ul class="product-list">
				
					<s:iterator value="lowQuantityProducts" var="product">
					<li>
						<div class="row">
							<div class="col-xs-4 col-sm-4 no-margin">
								<a href="#" class="thumb-holder"> <img width="73" height="73" alt=""
									src="<s:property value="img" />" />
								</a>
							</div>
							<div class="col-xs-8 col-sm-8 no-margin">
								<s:url action="show" var="url">
									<s:param name="pid">
										<s:property value="id" />
									</s:param>
								</s:url>
								<a href="<s:property value="#url"/>"><s:property value="title" /></a>
								<div class="price">
									<div class="price-current">
										$<s:property value="getText('{0,number,#,###.00}',{price})" />
									</div>
							
								</div>
							</div>
						</div>
					</li>
					</s:iterator>
					
				</ul>
			</div>
			<!-- /.widget -->
			<!-- <div class="widget">
				<div class="simple-banner">
					<a href="#"><img alt="" class="img-responsive"
						src="assets/images/blank.gif"
						data-echo="assets/images/banner/banner-simple.jpg" /></a>
				</div>
			</div> -->
			<!-- FEATURED PRODUCTS -->
			<div class="widget">
				<h1 class="border">Most Viewd</h1>
				<ul class="product-list">

					<s:iterator value="mostViewedProducts" var="product">
					<li>
						<div class="row">
							<div class="col-xs-4 col-sm-4 no-margin">
								<a href="#" class="thumb-holder"> <img width="73" height="73" alt=""
									src="<s:property value="img" />" />
								</a>
							</div>
							<div class="col-xs-8 col-sm-8 no-margin">
								<s:url action="show" var="url">
									<s:param name="pid">
										<s:property value="id" />
									</s:param>
								</s:url>
								<a href="<s:property value="#url"/>"><s:property value="title" /></a>
								<div class="price">
									<div class="price-current">
										$<s:property value="getText('{0,number,#,###.00}',{price})" />
									</div>
							
								</div>
							</div>
						</div>
					</li>
					</s:iterator>
					
				</ul>
				<!-- /.product-list -->
			</div>
			<!-- /.widget -->
			<!-- end /feature product -->
		</div><!-- end /sidebar -->

		<!-- CONTENT -->

		<div class="col-xs-12 col-sm-9 no-margin wide sidebar">

			<section id="recommended-products"
				class="carousel-holder hover small">

			<div class="title-nav">
				<h2 class="inverse">New Products</h2>
				<div class="nav-holder">
					<a href="#prev" data-target="#owl-recommended-products"
						class="slider-prev btn-prev fa fa-angle-left"></a> <a href="#next"
						data-target="#owl-recommended-products"
						class="slider-next btn-next fa fa-angle-right"></a>
				</div>
			</div>
			<!-- /.title-nav -->

			<div id="owl-recommended-products"
				class="owl-carousel product-grid-holder">
				
				<s:iterator value="newBrandProducts" var="product">
				<div class="no-margin carousel-item product-item-holder hover size-medium">
					<div class="product-item">
						<div class="ribbon blue">
							<span>new</span>
						</div>
						<div class="image">
							<img alt="" src="<s:property value="img" />" />
						</div>
						<div class="body">
							<div class="title">
								<s:url action="show" var="url">
									<s:param name="pid">
										<s:property value="id" />
									</s:param>
								</s:url>
								<a href="<s:property value="#url"/>"><s:property value="title" /></a>
							</div>
							<div class="brand"><s:property value="brand" /></div>
						</div>
						<div class="prices">
							<div class="price-current text-right">$<s:property value="getText('{0,number,#,###.00}',{price})" /></div>
						</div>
						<div class="hover-area">
							<div class="add-cart-button">
								<s:url action="show" var="url">
									<s:param name="pid">
										<s:property value="id" />
									</s:param>
								</s:url>
								<a href="<s:property value="#url"/>" class="le-button">add
									to cart</a>
							</div>
						</div>
					</div>
				</div>
				<!-- /.carousel-item -->
				</s:iterator>

			</div>
			<!-- /#recommended-products-carousel .owl-carousel --> </section>
			<!-- /.carousel-holder -->
			<section id="gaming">
			<div class="grid-list-products">
				<!-- <h2 class="section-title">Gaming</h2> -->

				<div class="control-bar">
					<h2 class="section-title">Product list</h2>
					<%-- <div id="popularity-sort" class="le-select">
						<select data-placeholder="sort by popularity">
							<option value="1">1-100 players</option>
							<option value="2">101-200 players</option>
							<option value="3">200+ players</option>
						</select>
					</div>

					<div id="item-count" class="le-select">
						<select>
							<option value="1">24 per page</option>
							<option value="2">48 per page</option>
							<option value="3">32 per page</option>
						</select>
					</div>

					<div class="grid-list-buttons">
						<ul>
							<li class="grid-list-button-item active"><a
								data-toggle="tab" href="#grid-view"><i
									class="fa fa-th-large"></i> Grid</a></li>
							<li class="grid-list-button-item "><a data-toggle="tab"
								href="#list-view"><i class="fa fa-th-list"></i> List</a></li>
						</ul>
					</div> --%>
				</div>
				<!-- /.control-bar -->

				<div class="tab-content">
					<div id="list-view" class="products-grid fade tab-pane in active">
						<div class="products-list">
							
							<s:iterator value="categoryProducts" var="product">
							<div class="product-item product-item-holder">
								<div class="row">
									<div class="no-margin col-xs-12 col-sm-4 image-holder">
										<div class="image">
											<img alt="" src="<s:property value="img" />"/>
										</div>
									</div>
									<!-- /.image-holder -->
									<div class="no-margin col-xs-12 col-sm-5 body-holder">
										<div class="body">
											<div class="title">
											
												<s:url action="show" var="url">
													<s:param name="pid">
														<s:property value="id" />
													</s:param>
												</s:url>
												<a href="<s:property value="#url"/>"><s:property value="title" /></a>
											</div>
											<div class="brand"><s:property value="brand" /></div>
											<div class="excerpt">
												<p><s:property value="description" /></p>
											</div>
										</div>
									</div>
									<!-- /.body-holder -->
									<div class="no-margin col-xs-12 col-sm-3 price-area">
										<div class="right-clmn">
											<div class="price-current">$<s:property value="getText('{0,number,#,###.00}',{price})" /></div>
											<div class="availability">
												<label>availability:</label>
												
													<s:if test="quantity > 0">
														<span class="available">in stock</span>
													</s:if>
													<s:else>
														<span class="not-available">out of stock</span>
													</s:else>
												
											</div>
											<s:url action="show" var="url">
												<s:param name="pid">
													<s:property value="id" />
												</s:param>
											</s:url>
											<a href="<s:property value="#url"/>" class="le-button">add
												to cart</a>
										</div>
									</div>
									<!-- /.price-area -->
								</div>
								<!-- /.row -->
							</div>
							<!-- /.product-item -->
							</s:iterator>
							
						</div>
						<!-- /.products-list -->

						<div class="pagination-holder">
							<div class="row">
								<!-- <div class="col-xs-12 col-sm-6 text-left">
									<ul class="pagination">
										<li class="current"><a href="#">1</a></li>
										<li><a href="#">2</a></li>
										<li><a href="#">3</a></li>
										<li><a href="#">4</a></li>
										<li><a href="#">next</a></li>
									</ul>
									/.pagination
								</div> -->
								<div class="col-xs-12 col-sm-6">
									<div class="result-counter">
										Showing all<%--  <span>1-9</span> of <span>11</span> results --%>
									</div>
									<!-- /.result-counter -->
								</div>
							</div>
							<!-- /.row -->
						</div>
						<!-- /.pagination-holder -->

					</div>
					<!-- /.products-grid #list-view -->

				</div>
				<!-- /.tab-content -->
			</div>
			<!-- /.grid-list-products --> </section>
			<!-- /#gaming -->
		</div>
		<!-- /.col -->
		<!-- end /CONTENT -->
	</div>
	<!-- /.container --> </section>
	<!-- /#category-grid -->
	
	
	<!--  FOOTER -->
	<s:include value="templates/footer.jsp" />
</body>
</html>