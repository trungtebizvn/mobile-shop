<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<s:include value="templates/head.jsp" />
<style>
.complete-page.margin-top-20.float-left {
	text-align: center;
}

.customer-notes {
	margin: 50px 0px;
}

.buttons-container {
	margin-top: 10px;
	float: right;
}

.checkout-shipping-block--title {
	font-size: 25px;
	font-weight: 600;
	color: green;
	border-bottom: 1px dotted #e4e4e4;
	margin-bottom: 20px;
	text-transform: uppercase;
}

.txt_thank_desc {
	font-size: 18px;
	font-weight: 600;
	margin-bottom: 5px;
	text-transform: uppercase;
}
</style>
</head>
<body>

	<div class="wrapper">
		<s:action name="header" executeResult="true">
		</s:action>
		<s:include value="templates/main-navigation.jsp" />

		<section id="checkout-page">
			<div class="container">

				<div class="complete-page margin-top-20 float-left">
					<div class="customer-notes" style="padding-top: 0;">
						<div class="checkout-shipping-block--title"
							style="padding-top: 0;">ORDER INFO</div>
						<div class="txt_thank_desc">Your order has been successfully
							processed</div>
						<div class="txt_thank_desc_2">
							Thank for your order. We will contact you soon for more
							infomation about shipping.<br> Invoice ID: <strong
								style="color: red">GTS11511229<s:property value="invoice.id" /></strong>
							<!-- <a href="http://www.vinabook.com/index.php?dispatch=orders.details&amp;order_id=47440592"> Chi tiáº¿t ÄÆ¡n hÃ ng</a>. -->
						</div>
						<div class="buttons-container">


							<span class="button button-wrap-left"><span
								class="button button-wrap-right"> 
								<s:url action="index" var="indexUrl" />
								<a class="simple-link block"
								href="<s:property value="indexUrl"/>"><i class="fa fa-long-arrow-left"></i> Continue shopping</a>
								</span></span>

						</div>
					</div>
				</div>


				<section id="your-order">
					<h2 class="border h1">your order</h2>
					<s:iterator value="products" var="product">
						<div class="row no-margin order-item">
							<div class="col-xs-12 col-sm-1 no-margin">
								<a href="#" class="qty"><s:property value="value" /> x</a>
							</div>

							<div class="col-xs-12 col-sm-9 ">
								<div class="title">
									<s:url action="show" var="url">
										<s:param name="pid">
											<s:property value="key.id" />
										</s:param>
									</s:url>
									<a href="<s:property value="#url"/>"><s:property
											value="key.title" /></a>
								</div>
								<div class="brand">
									<s:property value="key.brand" />
								</div>
							</div>

							<div class="col-xs-12 col-sm-2 no-margin">
								<s:set var="totalItem" value="%{value * key.price}" />
								<div class="price">
									$
									<s:property
										value="getText('{0,number,#,###.00}',{#attr.totalItem})" />
								</div>
							</div>
						</div>
						<!-- /.order-item -->
					</s:iterator>
				</section>
				<!-- /#your-order -->
				
				<div id="total-area" class="row no-margin">
					<div class="col-xs-12 col-lg-4 col-lg-offset-8 no-margin-right">
						<div id="subtotal-holder">
							<ul id="total-field" class="tabled-data inverse-bold ">
								<li><label>order total</label>
									<div class="value" style="color:red">
										$
										<s:property value="getText('{0,number,#,###.00}',{total})" />
									</div></li>
							</ul>
							<!-- /.tabled-data -->
						</div>
						<!-- /#subtotal-holder -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /#total-area -->
			</div>
			<!-- /.container -->
		</section>
		<!-- /#checkout-page -->
	</div>
	<!-- FOOTER -->
	<s:include value="templates/footer.jsp" />
</body>
</html>