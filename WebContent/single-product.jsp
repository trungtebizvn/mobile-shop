<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<s:include value="templates/head.jsp" />
</head>
<body>

	<div class="wrapper">
		<!--  HEADER  -->
		<s:action name="header" executeResult="true">
		</s:action>
		<s:include value="templates/main-navigation.jsp" />
		
		<!-- CONTENT -->
		<div id="single-product">
			<div class="container">

				<div class="no-margin col-xs-12 col-sm-6 col-md-5 gallery-holder">
					<div
						class="product-item-holder size-big single-product-gallery small-gallery">

						<div id="owl-single-product">
							<div class="single-product-gallery-item" id="slide1">
								<a data-rel="prettyphoto"
									href="images/products/product-gallery-01.jpg"> <img
									class="img-responsive" alt="" src="<s:property value="product.img"/>"
									data-echo="assets/images/products/product-gallery-01.jpg" />
								</a>
							</div>
							
						</div>
						<!-- /.single-product-slider -->
					</div>
					<!-- /.single-product-gallery -->
				</div>
				<!-- /.gallery-holder -->
				<div class="no-margin col-xs-12 col-sm-7 body-holder">
					<div class="body">
						<div class="star-holder inline">
							<div class="star" data-score="4"></div>
						</div>
						<div class="availability">
							<label>Availability:</label>
							<s:if test="product.quantity > 0">
								<span class="available">in stock</span>
							</s:if>
							<s:else>
								<span class="not-available">out of stock</span>
							</s:else>
						</div>

						<div class="title">
							<a href="#"><s:property value="product.title" /></a>
						</div>
						<div class="brand">
							<s:property value="product.brand" />
						</div>

						<div class="social-row">
							<span class="st_facebook_hcount"></span> <span
								class="st_twitter_hcount"></span> <span
								class="st_pinterest_hcount"></span>
						</div>
						<div class="excerpt">
							<s:property value="product.description" />
						</div>

						<div class="prices">
							<div class="price-current">
								$<s:property
									value="getText('{0,number,#,###.00}',{product.price})" />
								
							</div>
						</div>

						<div class="qnt-holder">
							<s:url action="addProduct" var="url">
								<s:param name="pid">
									<s:property value="product.id" />
								</s:param>
							</s:url>
							<a id="addto-cart" href="<s:property value="#url"/>"
								class="le-button huge">add to cart</a>
						</div>
						<!-- /.qnt-holder -->
					</div>
					<!-- /.body -->

				</div>
				<!-- /.body-holder -->
			</div>
			<!-- /.container -->
		</div>
		<!-- /.single-product -->

		<!-- SINGLE PRODUCT TAB -->
		<section id="single-product-tab">
		<div class="container">
			<div class="tab-holder">

				<ul class="nav nav-tabs simple">
					<li class="active"><a href="#description" data-toggle="tab">Description</a></li>
					<li><a href="#additional-info" data-toggle="tab">Additional
							Information</a></li>
				</ul>
				<!-- /.nav-tabs -->

				<div class="tab-content">
					<div class="tab-pane active" id="description">
						<p>
							<s:property value="product.description" />
						</p>
						<div class="meta-row">
							<div class="inline">
								
							</div>
							<!-- /.inline -->

							<%-- <span class="seperator">/</span> --%>

							<div class="inline">
								<label>categories:</label> <span><a href="#"><s:property
											value="product.brand" /></a></span>

							</div>
							<!-- /.inline -->
						</div>
						<!-- /.meta-row -->
					</div>
					<!-- /.tab-pane #description -->

					<div class="tab-pane" id="additional-info">
						<p class="specifications"><s:property value="product.specifications" /></p>

						<div class="meta-row">
							<div class="inline">
								<label>categories:</label> <span><a href="#"><s:property
											value="product.brand" /></a></span>
							</div>
							<!-- /.inline -->

						</div>
						<!-- /.meta-row -->
					</div>
					<!-- /.tab-pane #additional-info -->
				</div>
				<!-- /.tab-content -->

			</div>
			<!-- /.tab-holder -->
		</div>
		<!-- /.container --> </section>

		<!--  MOST VIEWED -->
		<s:action name="most-viewed" executeResult="true">
		</s:action>
		
		<!-- FOOTER  -->
		<s:include value="templates/footer.jsp" />
	</div>
	<!-- /.wrapper -->

</body>
</html>