<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<s:include value="templates/head.jsp" />
</head>
<body>

	<div class="wrapper">
		<s:action name="header" executeResult="true">
		</s:action>
		<s:include value="templates/main-navigation.jsp" />
		<div id="breadcrumb-alt">
			<div class="container">
				<div class="breadcrumb-nav-holder minimal">
					<ul>
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item current"><a href="#">shopping
								cart</a></li>
					</ul>
				</div>
				<!-- .breadcrumb-nav-holder -->
			</div>
			<!-- /.container -->
		</div>
		<!-- /#breadcrumb-alt -->
	</div>
	
	<!-- CONTENT  -->
	<section id="cart-page">
		<div class="container">
			
			<div class="col-xs-12 col-md-9 items-holder no-margin">


				<s:iterator value="products" var="product">

					<div class="row no-margin cart-item">
						<div class="col-xs-12 col-sm-2 no-margin">
							<a href="#" class="thumb-holder"> <img class="lazy" alt=""
								src="<s:property value="key.img"/>" />
							</a>
						</div>

						<div class="col-xs-12 col-sm-5">
							<div class="title">
								<s:url action="show" var="url">
									<s:param name="pid">
										<s:property value="key.id" />
									</s:param>
								</s:url>
								<a href="<s:property value="#url"/>"><s:property value="key.title" /></a>
							</div>
							<div class="brand">
								<s:property value="key.brand" />
							</div>
						</div>

						<div class="col-xs-12 col-sm-3 no-margin">
							<div class="quantity">
								<div class="le-quantity">
									<s:url action="reduceProduct" var="urlReduce">
										<s:param name="pid">
											<s:property value="key.id" />
										</s:param>
									</s:url>
									<s:url action="addProduct" var="urlAdd">
										<s:param name="pid">
											<s:property value="key.id" />
										</s:param>
									</s:url>

									<a class="minus" href="<s:property value="#urlReduce"/>"></a> <input
										name="quantity" readonly="readonly" type="text"
										value="<s:property value="value" />" /> <a class="plus"
										href="<s:property value="#urlAdd"/>"></a>
								</div>
							</div>
						</div>

						<div class="col-xs-12 col-sm-2 no-margin">
							<div class="price">
								<s:set var="totalItem" value="%{value * key.price}" />

								$
								<s:property
									value="getText('{0,number,#,###.00}',{#attr.totalItem})" />
							</div>
							<s:url action="removeFromCart" var="urlRemove">
								<s:param name="pid">
									<s:property value="key.id" />
								</s:param>
							</s:url>
							<a class="close-btn" href="<s:property value="#urlRemove"/>"></a>
						</div>
					</div>
					<!-- /.cart-item -->

				</s:iterator>

			</div>

			<!-- ========================================= SIDEBAR ========================================= -->

			<div class="col-xs-12 col-md-3 no-margin sidebar ">
				<div class="widget cart-summary">
					<h1 class="border">shopping cart</h1>
					<div class="body">
						<ul class="tabled-data no-border inverse-bold">
							<li><label>cart subtotal</label>
								<div class="value pull-right">
									$
									<s:property value="getText('{0,number,#,###}',{total})" />
								</div></li>
							<li><label>shipping</label>
								<div class="value pull-right">free shipping</div></li>
						</ul>
						<ul id="total-price" class="tabled-data inverse-bold no-border">
							<li><label>order total</label>
								<div class="value pull-right">
									$
									<s:property value="getText('{0,number,#,###.00}',{total})" />
								</div></li>
						</ul>
						<div class="buttons-holder">
							<s:url action="checkout" var="checkoutUrl" />
							<a class="le-button big" href="<s:property value="checkoutUrl"/>">checkout</a>

							<s:url action="index" var="indexUrl" />
							<a class="simple-link block"
								href="<s:property value="indexUrl"/>">continue shopping</a>
						</div>
					</div>
				</div>
				<!-- /.widget -->
			</div>
			<!-- /.sidebar -->

			<!-- ========================================= SIDEBAR : END ========================================= -->
		</div>
	</section>
	
	<s:include value="templates/footer.jsp" />
</body>
</html>