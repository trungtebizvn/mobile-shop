<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<s:include value="templates/head.jsp" />
</head>
<body>
	<!-- CONTENT  -->
	<main id="authentication" class="inner-bottom-md">
	<div class="container">
		<div class="row">

			<div class="col-md-6">
				<section class="section sign-in inner-right-xs">
					<h2 class="bordered">Sign In</h2>
					<p>Hello, Welcome to your account</p>

					<!-- <div class="social-auth-buttons">
						<div class="row">
							<div class="col-md-6">
								<button class="btn-block btn-lg btn btn-facebook"><i class="fa fa-facebook"></i> Sign In with Facebook</button>
							</div>
							<div class="col-md-6">
								<button class="btn-block btn-lg btn btn-twitter"><i class="fa fa-twitter"></i> Sign In with Twitter</button>
							</div>
						</div>
					</div> -->
					<s:url action="login" var="loginUrl" />
					<form role="form" class="login-form cf-style-1" method="post" action="<s:property value="loginUrl"/>" >
						<div class="field-row">
							<label>Username</label> <input type="text" name="username" class="le-input">
						</div>
						<!-- /.field-row -->

						<div class="field-row">
							<label>Password</label> <input type="password" name="password" class="le-input">
						</div>
						<!-- /.field-row -->

						<div class="field-row clearfix">
							<span class="pull-left"> <label class="content-color">
							<s:url action="index" var="indexUrl" />
								<span class="bold">
									<a href="<s:property value="indexUrl"/>"><i class="fa fa-long-arrow-left"></i> Go back to homepage</a>
								</span>
							</label>
							</span>
						</div>

						<div class="buttons-holder">
							<button type="submit" class="le-button huge">Sign in</button>
						</div>
						<!-- /.buttons-holder -->
					</form>
					<!-- /.cf-style-1 -->

				</section>
				<!-- /.sign-in -->
			</div>
			<!-- /.col -->



		</div>
		<!-- /.row -->
	</div>
	<!-- /.container --> </main>
	<!-- /.authentication -->
	
</body>
</html>