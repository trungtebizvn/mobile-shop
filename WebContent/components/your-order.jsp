<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<section id="your-order">
    <h2 class="border h1">your order</h2>
    	<s:iterator value="products" var="product">
         <div class="row no-margin order-item">
             <div class="col-xs-12 col-sm-1 no-margin">
                 <a href="#" class="qty"><s:property value="value"/> x</a>
             </div>

             <div class="col-xs-12 col-sm-9 ">
                 <div class="title">
	                 <s:url action="show" var="url">
						<s:param name="pid">
							<s:property value="key.id" />
						</s:param>
					</s:url>
					<a href="<s:property value="#url"/>"><s:property value="key.title" /></a>
                 </div>
                 <div class="brand"><s:property value="key.brand"/></div>
             </div>

             <div class="col-xs-12 col-sm-2 no-margin">
             	<s:set var="totalItem" value="%{value * key.price}" />
                 <div class="price">$<s:property value="getText('{0,number,#,###.00}',{#attr.totalItem})" /></div>
             </div>
         </div><!-- /.order-item -->
	</s:iterator>    
</section><!-- /#your-order -->