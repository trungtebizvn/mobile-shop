<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<s:include value="../templates/head.jsp" />
</head>
<body>

	<div class="wrapper">
		<s:include value="../templates/ad-header.jsp" />
		<!--  CONTENT  -->
		
		<section id="checkout-page">
		<div class="container">

			<div class="head">
				<h4
					style='margin: 30px 0px 20px 0px; font-size: 20px; font-weight: 600'>List
					Invoice</h4>
			</div>
			<table class="table table-striped table-bordered"
				id="my-orders-table">
				<thead>
					<tr class="first last">
						<th>Barcode</th>
						<th>Created at</th>
						<th>Amount</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					<s:iterator value="invoicesList" var="invoice">
						<tr class="first odd">
							<td>
								<s:url action="invoice-detail" var="urlDetail">
									<s:param name="iid">
										<s:property value="id" />
									</s:param>
								</s:url>
								<a href="<s:property value="urlDetail"/>" title="barcode">GTS115112<s:property value="id"/></a>
							</td>
							<td><s:date name="invoice.created_at"/></td>
							<td>$<s:property value="getText('{0,number,#,##0.00}',{amount})" /></td>
							<td>
								<s:url action="complete-invoice" var="completeUrl">
									<s:param name="iid">
										<s:property value="id" />
									</s:param>
								</s:url>
								<s:property value="status"/> 
								<s:if test="status == 'PENDING'">			
									<a href="<s:property value="completeUrl"/>"> <i class="fa fa-long-arrow-right"></i> COMPLETED</a>
								</s:if>
								
							</td>
	
						</tr>

					</s:iterator>
				</tbody>
			</table>

			<script type="text/javascript">
				decorateTable('my-orders-table');
			</script>

		</div>
		<!-- /.container --> </section>
		<!-- /#checkout-page -->
		<!-- ========================================= CONTENT : END ========================================= -->
		<!-- ============================================================= FOOTER ============================================================= -->
		<footer id="footer" class="color-bg">

		<div style='margin-top: 20px; text-align: center'>Design by
			LucNguyenTrungThanh</div>


		</footer>
		<!-- /#footer -->
		<!-- ============================================================= FOOTER : END ============================================================= -->
	</div>
	<!-- /.wrapper -->


	<!-- JavaScripts placed at the end of the document so the pages load faster -->
	<script src="assets/js/jquery-1.10.2.min.js"></script>
	<script src="assets/js/jquery-migrate-1.2.1.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script
		src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
	<script src="assets/js/gmap3.min.js"></script>
	<script src="assets/js/bootstrap-hover-dropdown.min.js"></script>
	<script src="assets/js/owl.carousel.min.js"></script>
	<script src="assets/js/css_browser_selector.min.js"></script>
	<script src="assets/js/echo.min.js"></script>
	<script src="assets/js/jquery.easing-1.3.min.js"></script>
	<script src="assets/js/bootstrap-slider.min.js"></script>
	<script src="assets/js/jquery.raty.min.js"></script>
	<script src="assets/js/jquery.prettyPhoto.min.js"></script>
	<script src="assets/js/jquery.customSelect.min.js"></script>
	<script src="assets/js/wow.min.js"></script>
	<script src="assets/js/scripts.js"></script>
</body>
</html>