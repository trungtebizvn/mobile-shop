<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<s:include value="../templates/head.jsp" />
</head>
<body>

	<div class="wrapper">
		<s:include value="../templates/ad-header.jsp"/>
		<!--  CONTENT  -->
		<section id="checkout-page">
		<div class="container">

			<div class="head">
				<h4
					style='margin: 30px 0px 20px 0px; font-size: 20px; font-weight: 600'>List
					Product</h4>
				<a style='float: right' href="#">Show all</a>
			</div>
			<table class="table table-striped table-bordered"
				id="my-orders-table">
				<thead>
					<tr class="first last">
						<th>Name</th>
						<th>Brand</th>
						<th>Quantity</th>
						<th>Price</th>
						<th>Show</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<s:iterator value="productsList" var="product">
						<tr class="first odd">
							<td><a href="#" title="title"><s:property value="title"/></a></td>
							<td><s:property value="brand"/></td>
							<td><s:property value="quantity"/></td>
							<td>
								$<s:property value="getText('{0,number,#,##0.00}',{price})" />
							</td>
							<td>
								<s:url action="change-status" var="url">
									<s:param name="pid">
										<s:property value="id" />
									</s:param>
								</s:url>
								<a href="<s:property value="#url"/>" title="status">
									<s:if test="status == 1">
										<input type="checkbox" checked="checked" disabled> Hide
									</s:if>
									<s:else>
										<input type="checkbox" disabled> Show
									</s:else>
									
								</a>
							</td>
							<td class="last">
								<s:url action="ad-show-product" var="editingUrl">
									<s:param name="pid">
										<s:property value="id" />
									</s:param>
								</s:url>
								<s:url action="delete-product" var="deleteUrl">
									<s:param name="pid">
										<s:property value="id" />
									</s:param>
								</s:url>
								<a style="margin-left:20px" href="<s:property value="editingUrl"/>" title="Edit"><i class="fa fa-pencil-square-o fa-2x"></i></a>
								
								<a style="margin-left:20px" href="<s:property value="deleteUrl"/>" title="Delete"><i class="fa fa-trash-o fa-2x"></i></a>
								</td>
						</tr>
					</s:iterator>
				</tbody>
			</table>

			<script type="text/javascript">
				decorateTable('my-orders-table');
			</script>

		</div>
		<!-- /.container --> </section>
		<!-- /#checkout-page -->

	</div>
	<!-- /.wrapper -->


</body>
</html>