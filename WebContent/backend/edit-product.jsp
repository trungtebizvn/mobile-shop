<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<s:include value="../templates/head.jsp" />
</head>
<body>

	<div class="wrapper">

		<s:include value="../templates/ad-header.jsp" />

		<section id="checkout-page">
		<div class="container">

			<div class="head">
				<div class="col-md-6">
					<section class="section register inner-left-xs"> <!-- <h2 class="bordered">Edit Product</h2> -->
					<s:url action="update-product" var="url">
						<s:param name="pid">
							<s:property value="product.id" />
						</s:param>
					</s:url>
					<form action="<s:property value="url"/>" role="form" class="register-form cf-style-1" method="post">
						<div class="field-row">
							<label>Title</label> <input type="text" name='product.title'
								class="le-input" value="<s:property value="product.title"/>"
								placeholder='Input Title'>
						</div>
						<!-- /.field-row -->

						<div class="field-row">
							<label>Brand</label> <input type="text" name='product.brand'
								class="le-input" placeholder='Input Brand'
								value="<s:property value="product.brand"/>">
						</div>
						<!-- /.field-row -->

						<div class="field-row">
							<label>Price</label> <input type="number" name='product.price'
								class="le-input" placeholder='Input Price'
								value="<s:property value="product.price"/>">
						</div>
						<!-- /.field-row -->

						<div class="field-row">
							<label>Quantity</label> 
							<input type="text" disabled 
								class="le-input" placeholder='Input Quantity'
								value="<s:property value="product.quantity"/>">
							<input type="number" class="le-input" name='product.quantity' placeholder='Add quantity'>
							
						</div>
						<!-- /.field-row -->
						
						<div class="field-row">
							
							
						</div>
						<!-- /.field-row -->

						<div class="field-row">
							<label>Description</label>
							<textarea rows='5' cols='60' name='product.description' class="le-input">
								<s:property value="product.description"/>
							</textarea>
						</div>
						<!-- /.field-row -->

						<div class="field-row">
							<label>Specifications</label> 
							<textarea rows='5' cols='60'
								name='product.specifications' class="le-input"
								placeholder='Input Specifications'>
								<s:property value="product.specifications"/>
							</textarea>
						</div>
						<!-- /.field-row -->


						<div class="field-row">
							<label>Size</label> <input type="text" name='product.size'
								class="le-input" placeholder='Input Size'
								value="<s:property value="product.size"/>">
						</div>
						<!-- /.field-row -->

						<div class="field-row">
							<label>Promotion</label> <input type="text" name='product.promotion'
								class="le-input" placeholder='Input Promotion'
								value="<s:property value="product.promotion"/>">
						</div>
						<!-- /.field-row -->



						<div class="buttons-holder">
							<s:url action="man-product-show" var="url"></s:url>
							<a style="margin:20px" class="le-button inverse" href="<s:property value="url"/>">Cancel</a>	
							<button type="submit" class="le-button small">Update</button>
						</div>
						<!-- /.buttons-holder -->
					</form>



					</section>
					<!-- /.register -->

				</div>
				<!-- /.col -->

				<script type="text/javascript">
					decorateTable('my-orders-table');
				</script>

			</div>
			<!-- /.container -->
		</section>
		<!-- /#checkout-page -->
	</div>
	<!-- /.wrapper -->

</body>
</html>