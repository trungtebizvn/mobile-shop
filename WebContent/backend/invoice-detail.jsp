<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<s:include value="../templates/head.jsp" />
</head>
<body>

	<div class="wrapper">
		<s:include value="../templates/ad-header.jsp" />
		<!--  CONTENT  -->
		<section id="checkout-page">
		<div class="container">
			<h4
				style='margin: 30px 0px 20px 0px; font-size: 20px; font-weight: 600'>Invoice barcode : <strong style="color:red">GTS115112<s:property value="invoice.id"/></strong></h4>

			<table class="table table-striped table-bordered"
				id="my-orders-table">
				<thead>
					<tr class="first last">
						<th>#</th>
						<th>Product</th>
						<th>Quantity</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody>

					<s:iterator value="invoiceDetails" var="detail" status="loop" step="1">
						<tr class="first odd">
							<s:set var="index" value="%{#loop.index}" />
							<td><s:property
										value="%{#index+1}" />
							<td><s:property
										value="#attr.productsList.get(#index).title" />
							</td>
							<td><s:property value="quantity" /></td>
							<td>$<s:property
									value="getText('{0,number,#,##0.00}',{amount})" /></td>

						</tr>

					</s:iterator>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="5" class="a-right">Delivery fee</td>
						<td class="last a-right"><span class="price">$0.00</span></td>
					</tr>
					<tr class="last">
						<td colspan="5" class="a-right">Total order</td>
						<td class="last a-right"><span class="price">$<s:property
									value="getText('{0,number,#,##0.00}',{invoice.amount})" /></span></td>
					</tr>
				</tfoot>
			</table>
			<div>
				<h3>Customer</h3>
				<div>
					<s:property value="#attr.customer.name" />
				</div> 
				<div>
					<s:property value="#attr.customer.phone" />
				</div> 
				<div>
					<s:property value="#attr.customer.email" />
				</div> 
				<div>
					<s:property value="#attr.customer.address" />
				</div> 
			</div>
			
			<script type="text/javascript">
				decorateTable('my-orders-table', {
					'tbody' : [ 'odd', 'even' ],
					'tbody tr' : [ 'first', 'last' ]
				})
			</script>
			<div class="button-set">
			<s:url action="man-invoice-show" var="url"></s:url>
				<a href="<s:property value="url"/>" class="left">« Go back</a> <input
					value="WtFKUTmgYoYuonSH" name="order_form_key" id="order_form_key"
					type="hidden">
			</div>

		</div>
		<!-- /.container --> 
		</section>
	</div>
	<!-- /.wrapper -->

</body>
</html>