<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<s:include value="templates/head.jsp" />
</head>
<body>

	<div class="wrapper">
		<s:action name="header" executeResult="true">
		</s:action>
		<s:include value="templates/main-navigation.jsp" />
	</div>
	<!-- ========================================= CONTENT ========================================= -->

	<section id="checkout-page">
		<s:form action="create" method="post" validate="true">
			<div class="container">
				<div class="col-xs-12 no-margin">
					<div class="billing-address">
						<h2 class="border h1">billing address</h2>

						<div class="row field-row">
							<div class="col-xs-12 col-sm-12">
								<label>full name*</label> <input class="le-input"
									name="customer.name">
							</div>
						</div>
						<!-- /.field-row -->
						<div class="row field-row">
							<div class="col-xs-12 col-sm-12">
								<label>shipping address*</label> <input class="le-input"
									data-placeholder="street address, town" name="customer.address">
							</div>
						</div>
						<!-- /.field-row -->

						<div class="row field-row">
							<div class="col-xs-12 col-sm-6">
								<label>email address*</label> <input class="le-input"
									name="customer.email">
							</div>

							<div class="col-xs-12 col-sm-6">
								<label>phone number*</label> <input class="le-input"
									name="customer.phone">
							</div>
						</div>
						<!-- /.field-row -->

						<!-- <div class="row field-row">
							<div id="create-account" class="col-xs-12">
								<input class="le-checkbox big" type="checkbox" /> <a
									class="simple-link bold" href="#">Create Account?</a> - you
								will receive email with temporary generated password after login
								you need to change it.
							</div>
						</div> -->
						<!-- /.field-row -->

					</div>
					<!-- /.billing-address -->

					<s:action name="your-order" executeResult="true">
					</s:action>

					<div id="total-area" class="row no-margin">
						<div class="col-xs-12 col-lg-4 col-lg-offset-8 no-margin-right">
							<div id="subtotal-holder">
								<ul id="total-field" class="tabled-data inverse-bold ">
									<li><label>order total</label>
										<div class="value">
											$
											<s:property value="getText('{0,number,#,###.00}',{total})" />
										</div></li>
								</ul>
								<!-- /.tabled-data -->
							</div>
							<!-- /#subtotal-holder -->
						</div>
						<!-- /.col -->
					</div>
					<!-- /#total-area -->

					<div class="place-order-button">
						<button type="submit" class="le-button big">place order</button>
					</div>
					<!-- /.place-order-button -->

				</div>
				<!-- /.col -->
			</div>
			<!-- /.container -->
		</s:form>
	</section>
	<!-- /#checkout-page -->
	
	<s:include value="templates/footer.jsp" />
</body>
</html>