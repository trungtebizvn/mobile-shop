<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<s:include value="templates/head.jsp" />
</head>
<body>
	<div class="wrapper">
	
		<!--  HEADER  -->
 		<s:action name="header" executeResult="true">
		</s:action>
		
		<!--  BANNER - SIDE NAVIGATION -->
		<s:include value="templates/top.jsp" />

		<!--  CONTENT -->
		<div id="products-tab" class="wow fadeInUp">
			<div class="container">
				<div class="tab-holder">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs">
						<li class="active"><a href="#featured" data-toggle="tab">Featured</a></li>
						<li><a href="#new-arrivals" data-toggle="tab">New</a></li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="featured">
							<div class="product-grid-holder">

								<s:iterator value="firstProducts" var="product">
									<div
										class="col-sm-4 col-md-3 no-margin product-item-holder hover">
										<div class="product-item">
											<div class="ribbon red">
												<span>sale</span>
											</div>
											<div class="image">			
												<img alt="logo" src="<s:property value="img"/>" width="100%" height="20%"/>
											</div>
											<div class="body">
												<div class="label-discount clear"></div>
												<div class="title">
													<s:url action="show" var="url">
														<s:param name="pid">
															<s:property value="id" />
														</s:param>
													</s:url>
													<a href="<s:property value="#url"/>"><s:property value="title" /></a>
												</div>
												<div class="brand">
													<s:property value="brand" />
												</div>
											</div>
											<div class="prices">
												<div class="price-current pull-right">
													$<s:property value="getText('{0,number,#,##0.00}',{price})" />
												</div>
											</div>
											<div class="hover-area">
												<div class="add-cart-button">

													<s:url action="show" var="url">
														<s:param name="pid">
															<s:property value="id" />
														</s:param>
													</s:url>
													<a href="<s:property value="#url"/>" class="le-button">add
														to cart</a>

												</div>
											</div>
										</div>
									</div>
								</s:iterator>
							</div>
						</div>
						<div class="tab-pane" id="new-arrivals">
							<div class="product-grid-holder">

								<s:iterator value="newProducts" var="product">
									<div
										class="col-sm-4 col-md-3 no-margin product-item-holder hover">
										<div class="product-item">
											<div class="ribbon blue">
												<span>new!</span>
											</div>
											<div class="image">
												<img alt="logo" src="<s:property value="img"/>" width="100%" height="20%"/>
											</div>
											<div class="body">
												<div class="label-discount clear"></div>
												<div class="title">
													<s:url action="show" var="url">
														<s:param name="pid">
															<s:property value="id" />
														</s:param>
													</s:url>
													<a href="<s:property value="#url"/>"><s:property value="title" /></a>
												</div>
												<div class="brand">
													<s:property value="brand" />
												</div>
											</div>
											<div class="prices">
												<div class="price-current pull-right">
													$<s:property value="getText('{0,number,#,###.00}',{price})" />
													
												</div>
											</div>
											<div class="hover-area">
												<div class="add-cart-button">
													<s:url action="show" var="url">
														<s:param name="pid">
															<s:property value="id" />
														</s:param>
													</s:url>
													<a href="<s:property value="#url"/>" class="le-button">add
														to cart</a>
												</div>
											</div>
										</div>
									</div>
									<!-- end /.product-item-holder -->
								</s:iterator>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!--  MOST VIEWED  -->
		<s:action name="most-viewed" executeResult="true">
		</s:action>
		
		<!--  FOOTER  -->
		<s:include value="templates/footer.jsp" />
	</div>
	<!-- /.wrapper -->
</body>
</html>