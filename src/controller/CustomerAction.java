package controller;

import java.util.Map;

import model.CustomerModel;
import model.InvoiceModel;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import entities.Cart;
import entities.Customer;
import entities.Product;

public class CustomerAction extends ActionSupport{
	
	private int pid;
	private CustomerModel cm = new CustomerModel();
	private Map<Product, Integer> products;
	private Customer customer = new Customer();
    private double total;
    
	@Override
	public String execute() throws Exception {
		// TODO Auto-generated method stub
		Cart cart = (Cart) ActionContext.getContext().getSession().get("cart");
        if (cart == null) {
            return ERROR;
        }
        products = cart.getProducts();
        total = cart.getTotalPrice();
        return SUCCESS;
	}
	
	@SuppressWarnings("unchecked")
	public String create(){
		int ctm_id = this.cm.insert(this.getCustomer());
		if( ctm_id == 0 ) {
			System.out.println("Create customer is failed.");	
			return ERROR;
		}
		System.out.println("Create customer is successful.");
		//Create new invoice
		int invoice_id = (new InvoiceModel()).insert(ctm_id);
		ActionContext.getContext().getSession().put("invoice_id", invoice_id);
		System.out.println("Invoice after create have id (session): " + (Integer)ActionContext.getContext().getSession().get("invoice_id"));
		
		
		return SUCCESS;
		
	}
	

	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public Map<Product, Integer> getProducts() {
		return products;
	}

	public void setProducts(Map<Product, Integer> products) {
		this.products = products;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(float total) {
		this.total = total;
	}



	public Customer getCustomer() {
		return customer;
	}



	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	
	
}
