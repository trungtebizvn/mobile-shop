package controller;

import java.util.Map;

import model.InvoiceModel;
import model.ProductModel;
import services.Emailer;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import entities.Cart;
import entities.Invoice;
import entities.Product;

public class CartAction extends ActionSupport {
	private int pid;
	private ProductModel pm = new ProductModel();
	private InvoiceModel im = new InvoiceModel();
	private Map<Product, Integer> products;
	private double total;
	private int amount;
	private Invoice invoice = new Invoice();

	public void setPid(int pid) {
		this.pid = pid;
	}

	@Override
	public String execute() throws Exception {
		Cart cart = (Cart) ActionContext.getContext().getSession().get("cart");
		this.amount = cart == null ? 0 : cart.getAmount();
		if (cart == null) {
			return ERROR;
		}

		Invoice inv = (Invoice) ActionContext.getContext().get("invoice");

		if (inv != null)
			this.invoice = inv;
		this.products = cart.getProducts();
		this.total = cart.getTotalPrice();

		return SUCCESS;
	}

	// Completed Order and Remove cart
	public String complete() {
		Cart cart = (Cart) ActionContext.getContext().getSession().get("cart");
		this.amount = cart == null ? 0 : cart.getAmount();
		if (cart == null) {
			return ERROR;
		}

		this.invoice = this.im.findById((Integer)ActionContext.getContext().getSession().get("invoice_id"));
		System.out.print("Invoice id session test " + invoice.getId());
		
		this.products = cart.getProducts();
		this.total = cart.getTotalPrice();
		
		//Send email
		Emailer emailer = new Emailer();
		emailer.send();
		
		ActionContext.getContext().getSession().remove("cart");
		ActionContext.getContext().getSession().remove("invoice_id");

		return SUCCESS;
	}

	// Addition a product to cart
	public String add() {
		Cart cart = (Cart) ActionContext.getContext().getSession().get("cart");
		if (cart == null) {
			cart = new Cart();
		}
		Product p = this.pm.findById(pid);
		if (p.getId() == 0) {
			return ERROR;
		}
		cart.addProduct(p);
		ActionContext.getContext().getSession().put("cart", cart);
		return SUCCESS;
	}

	// Reduce quantity of product
	public String reduce() {
		Cart cart = (Cart) ActionContext.getContext().getSession().get("cart");
		if (cart == null) {
			cart = new Cart();
		}
		Product p = this.pm.findById(pid);
		if (p.getId() == 0) {
			return ERROR;
		}
		cart.reduceProduct(p);
		ActionContext.getContext().getSession().put("cart", cart);
		return SUCCESS;
	}

	// Remove a product from cart
	public String remove() {
		Cart cart = (Cart) ActionContext.getContext().getSession().get("cart");
		if (cart == null) {
			return ERROR;
		}
		Product p = this.pm.findById(pid);
		if (cart.removeProduct(p)) {
			return SUCCESS;
		}
		return ERROR;
	}

	public Map<Product, Integer> getProducts() {
		return products;
	}

	public double getTotal() {
		return total;
	}

	public int getAmount() {

		return amount;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

}
