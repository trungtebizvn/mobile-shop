package controller;

import java.util.ArrayList;
import java.util.List;

import model.ProductModel;

import com.opensymphony.xwork2.ActionSupport;

import entities.Product;

public class ProductAction extends ActionSupport {
	private Product product = new Product();
	private List<Product> productsList = new ArrayList<Product>();
	private List<Product> newProducts = new ArrayList<Product>();
	
	private List<Product> firstProducts = new ArrayList<Product>();
	private List<Product> mostViewedProducts = new ArrayList<Product>();
	//special offers products
	private List<Product> lowQuantityProducts = new ArrayList<Product>();
	//Array product by brand and price is ascending
		private List<Product> categoryProducts = new ArrayList<Product>();
		//Array new product by brand
		private List<Product> newBrandProducts = new ArrayList<Product>();
	
	private ProductModel pm = new ProductModel();
	private int pid;
	private String brand;
	
	public void setPid(int pid) {
        this.pid = pid;
    }
	
	public String delete(){
		if( this.pm.delete(this.pid) )
			return SUCCESS;
		return ERROR;
	}
	public String browse(){
		this.categoryProducts = this.pm.findByBrand(brand);
		this.productsList = this.pm.findAll();
		this.lowQuantityProducts = this.pm.findLowQuantity(5);
		this.newProducts = this.pm.findNew(8);
		this.mostViewedProducts = this.pm.findMostViewed(10);
		this.newBrandProducts = this.pm.findNewByBrand(brand, 6);
		return SUCCESS;
	}
	
	public String execute(){
		this.productsList = this.pm.findAll();
		this.newProducts = this.pm.findNew(8);
		this.firstProducts = this.pm.findFirst(16);
		this.mostViewedProducts = this.pm.findMostViewed(10);
		return SUCCESS;

	}
	
	public String changeStatus(){
		this.pm.changeStatus(pid);
		return SUCCESS;
	}

	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public List<Product> getProductsList() {
		return productsList;
	}
	public void setProductsList(List<Product> productsList) {
		this.productsList = productsList;
	}

	public List<Product> getNewProducts() {
		return newProducts;
	}

	public void setNewProducts(List<Product> newProducts) {
		this.newProducts = newProducts;
	}
	
	public String show(){
		this.product = this.pm.findById(pid);
		return SUCCESS;
	}
	
	public String update(){
		if ( this.pm.update(this.product, this.pid) == SUCCESS)
			return SUCCESS;
		return ERROR;
	}

	public List<Product> getFirstProducts() {
		return firstProducts;
	}

	public void setFirstProducts(List<Product> firstProducts) {
		this.firstProducts = firstProducts;
	}

	public List<Product> getMostViewedProducts() {
		return mostViewedProducts;
	}

	public void setMostViewedProducts(List<Product> mostViewedProducts) {
		this.mostViewedProducts = mostViewedProducts;
	}

	public List<Product> getCategoryProducts() {
		return categoryProducts;
	}

	public void setCategoryProducts(List<Product> categoryProducts) {
		this.categoryProducts = categoryProducts;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public List<Product> getLowQuantityProducts() {
		return lowQuantityProducts;
	}
	public void setLowQuantityProducts(List<Product> lowQuantityProducts) {
		this.lowQuantityProducts = lowQuantityProducts;
	}
	public List<Product> getNewBrandProducts() {
		return newBrandProducts;
	}
	public void setNewBrandProducts(List<Product> newBrandProducts) {
		this.newBrandProducts = newBrandProducts;
	}
}
