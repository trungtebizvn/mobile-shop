package controller;

import java.util.ArrayList;
import java.util.List;

import model.CustomerModel;
import model.InvoiceDetailModel;
import model.InvoiceModel;
import model.ProductModel;

import com.opensymphony.xwork2.ActionSupport;

import entities.Customer;
import entities.Invoice;
import entities.InvoiceDetail;
import entities.Product;

public class InvoiceAction extends ActionSupport{
	private List<InvoiceDetail> invoiceDetails 	= new ArrayList<InvoiceDetail>();
	private List<Invoice> 		invoicesList 	= new ArrayList<Invoice>();
	private Invoice 			invoice			= new Invoice();
	private InvoiceModel 		im 				= new InvoiceModel();
	private InvoiceDetailModel 	idm 			= new InvoiceDetailModel();
	private ProductModel 		pm				= new ProductModel();
	private CustomerModel		cm				= new CustomerModel();
	private List<Product>		productsList	= new ArrayList<Product>();
	private Customer			customer		= new Customer();
	private int iid;
	
	@Override
	public String execute() throws Exception {
		this.setInvoicesList(this.im.findAll());
		this.setInvoiceDetails(this.idm.findByInvoiceId(this.iid));
		this.setInvoice(this.im.findById(this.iid));
		this.setProductsList(this.pm.findViaInvoice(this.invoiceDetails));
		this.setCustomer(this.cm.findById(invoice.getCustomerId()));
		System.out.println("Invoice ID -" + this.iid);
		

		return SUCCESS;
	}
	
	public String complete(){
		if( this.im.changeStatus(this.iid) )
			return SUCCESS;
		return ERROR;
	}
	
	public List<Invoice> getInvoicesList() {
		return invoicesList;
	}

	public void setInvoicesList(List<Invoice> invoicesList) {
		this.invoicesList = invoicesList;
	}

	public List<InvoiceDetail> getInvoiceDetails() {
		return invoiceDetails;
	}

	public void setInvoiceDetails(List<InvoiceDetail> invoiceDetails) {
		this.invoiceDetails = invoiceDetails;
	}
	public int getIid() {
		return iid;
	}
	public void setIid(int iid) {
		this.iid = iid;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public List<Product> getProductsList() {
		return productsList;
	}

	public void setProductsList(List<Product> productsList) {
		this.productsList = productsList;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	
}
