package controller;

import model.UserModel;

import com.opensymphony.xwork2.ActionSupport;

public class UserAction extends ActionSupport{
	private String username;
	private String password;
	private UserModel um = new UserModel();
	
	@Override
	public String execute() throws Exception {
		// TODO Auto-generated method stub
		if( this.um.authentication(username, password))
			return SUCCESS;
		System.out.println("Username : " + username + "\\" + password);
		return ERROR;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
