package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class UserModel extends ModelBase{
	public boolean authentication(String username, String password){
		try { 
			 this.configDB();
	         String sql = "SELECT * FROM user WHERE username = ? AND password = ?";
	         PreparedStatement ps = conn.prepareStatement(sql);
	         ps.setString(1, username);
	         ps.setString(2, password);
	         ResultSet rs = ps.executeQuery();

	         while (rs.next()) {
	        	 return true;
	         }
	      } catch (Exception e) {
	         System.out.println("Can't login, because : " + e.getMessage());
	      } finally {
	         if (conn != null) {
	            try {
	               conn.close();
	            } catch (Exception e) {
	            }
	         }
	      }
		return false;
	}
}
