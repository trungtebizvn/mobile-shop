package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.opensymphony.xwork2.ActionContext;

import entities.Cart;
import entities.Invoice;
import entities.Product;

public class InvoiceModel extends ModelBase{
	
	public boolean changeStatus(int iid){
		String status = "COMPLETED";
		try {
			this.configDB();
			String sql = "UPDATE invoice SET status = ?  WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, status);
			ps.setInt(2, iid);
			if( ps.executeUpdate() > 0 )
				return true;
			
		} catch (Exception e) {
			System.out.println("Updates status is failed. Because " + e.getMessage());
		}
		return false;
	}
	
	public Invoice findById(int id) {
		try { 
			 this.configDB();
	         String sql = "SELECT * FROM invoice WHERE id = ? ";
	         PreparedStatement ps = conn.prepareStatement(sql);
	         ps.setInt(1, id);
	         ResultSet rs = ps.executeQuery();

	         while (rs.next()) {
	        	 Invoice p = new Invoice();
	        	 p.setId(rs.getInt("id"));
	        	 p.setAmount(rs.getDouble("amount"));
	        	 p.setCustomerId(rs.getInt("customer_id"));
	        	 p.setStatus(rs.getString("status"));
	        	 p.setCreatedAt(rs.getDate("created_at"));
	        	 System.out.println("You just get invoice with id : " + p.getId());
	        	 return p;
	         }
	      } catch (Exception e) {
	         System.out.println("Can't get invoice by id : " + e.getMessage());
	      } finally {
	         if (conn != null) {
	            try {
	               conn.close();
	            } catch (Exception e) {
	            }
	         }
	      }
		return new Invoice(0, 0, 0, "");
	}
	
	public List<Invoice> findAll(){
		List<Invoice> invoicesList = new ArrayList<Invoice>();
		try { 
			 this.configDB();
	         String sql = "SELECT * FROM invoice";
	         PreparedStatement ps = conn.prepareStatement(sql);
	         ResultSet rs = ps.executeQuery();

	         while (rs.next()) {
	        	 Invoice p = new Invoice();
	        	 p.setId(rs.getInt("id"));
	        	 p.setAmount(rs.getDouble("amount"));
	        	 p.setCustomerId(rs.getInt("customer_id"));
	        	 p.setStatus(rs.getString("status"));
	        	 p.setCreatedAt(rs.getDate("created_at"));
	        	 invoicesList.add(p);
//	        	 System.out.println("Invoice ID - Created At: " + rs.getDate("created_at") + " - " + p.getCreatedAt());
	         }
	      } catch (Exception e) {
	         System.out.println("Can't get all invoice : " + e.getMessage());
	      } finally {
	         if (conn != null) {
	            try {
	               conn.close();
	            } catch (Exception e) {
	            }
	         }
	      }
		return invoicesList;
	}
	
	public int insert(int customer_id){
		this.configDB();
		int invoice_id = 0;
		try { 
			 this.configDB();
	         String sql = "INSERT INTO invoice values(NULL,?,?,?,?)";
	         PreparedStatement ps = conn.prepareStatement(sql ,Statement.RETURN_GENERATED_KEYS);
	         ps.setDouble(1, this.getAmount());
	         ps.setInt(2, customer_id);
	         ps.setString(3, (new Invoice()).STATUS_PENDING);
	         ps.setString(4, this.getDateToday());
	         if(ps.executeUpdate() > 0){
	        	//Get id of customer just insert
		         ResultSet rs = ps.getGeneratedKeys();  
	             if(rs.next())
	             {
	            	 invoice_id = rs.getInt(1); 
	                 System.out.println("Invoice id just insert :  " + invoice_id);
	                 (new InvoiceDetailModel()).insert(invoice_id);
	             }
	         }
	         
	      } catch (Exception e) {
	         System.out.println("Insert invoice is failed. Because  " + e.getMessage());
	      } finally {
	         if (this.conn != null) {
	            try {
	               this.conn.close();
	            } catch (Exception e) {
	            }
	         }
	      }
		return invoice_id;
	}

	public double getAmount(){
		Cart cart = (Cart) ActionContext.getContext().getSession().get("cart");
        if (cart == null) {
            return 0;
        }
        return cart.getTotalPrice();
	}
}
