package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import entities.InvoiceDetail;
import entities.Product;

public class ProductModel extends ModelBase{

	public void subtractQuantity(Product p, int pquantity){
		try {
			this.configDB();
			int quantity = this.getQuantity(p.getId()) - pquantity;
			String sql = "UPDATE product SET quantity = ? WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, quantity);
			ps.setInt(2, p.getId());
			ps.executeUpdate();
			
		} catch (Exception e) {
			System.out.println("Can't subtract quantity of the product by id. Because " + e.getMessage());
		}
	}
	public void changeStatus(int pid){
		Product p = this.findById(pid);
		int bool = p.getStatus() == 1 ? 0 : 1;
		try {
			this.configDB();
			String sql = "UPDATE product SET status = ? WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, bool);
			ps.setInt(2, pid);
			ps.executeUpdate();
			
		} catch (Exception e) {
			System.out.println("Updates status is failed. Because " + e.getMessage());
		}
	}
	public boolean addQuantity(int qty, int pid){
		int quantity = qty + this.getQuantity(pid);
		
		try {
			this.configDB();
			String sql = "UPDATE product SET quantity = ? WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, quantity);
			ps.setInt(2, pid);
			if(ps.executeUpdate() > 0)
				return true;
			
		} catch (Exception e) {
			System.out.println("Updates quantiy is failed. Because " + e.getMessage());	
		}
		return false;
	}
	public String update(Product p, int pid){
		
		try {
			this.configDB();
			String sql = "UPDATE product SET";
			sql += " title = ?, brand = ?, price = ?, description = ?, specifications = ?, size = ?, promotion = ?";
			sql += " WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, p.getTitle());
			ps.setString(2, p.getBrand());
			ps.setDouble(3, p.getPrice());
			ps.setString(4, p.getDescription());
			ps.setString(5, p.getSpecifications());
			ps.setFloat(6, p.getSize());
			ps.setString(7, p.getPromotion());
			ps.setInt(8, pid);
			if(ps.executeUpdate() > 0){
				this.addQuantity(p.getQuantity(), pid);
				return SUCCESS;
			}
			
		} catch (Exception e) {
			System.out.println("Updates product is failed. Because " + e.getMessage());	
		}
		return ERROR;
	}
	public boolean delete(int pid){
		
		try {
			this.configDB();
			String sql = "DELETE FROM product WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, pid);
			if(ps.executeUpdate() > 0)
				return true;
			
		} catch (Exception e) {
			System.out.println("Delete product is failed. Because " + e.getMessage());	
		}
		return false;
	}
	
	public int getQuantity(int pid){
		int quantity = 0;
		System.out.println("Product p have id  : " + pid);
		try {
			this.configDB();
			String sql = "SELECT quantity FROM product WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, pid);
			ResultSet rs = ps.executeQuery();

	         while (rs.next()) {
	        	 quantity = rs.getInt("quantity");
	         }
			
		} catch (Exception e) {
			System.out.println("Can't subtract quantity of the product by id. Because " + e.getMessage());
		}
		System.out.println("QTY : " + quantity);
		return quantity;
	}
	public List<Product> findMostViewed(int count){
		List<Product> productsList = new ArrayList<Product>();
		try { 
			 this.configDB();
	         String sql = "SELECT * FROM product WHERE status = 1 ORDER BY view DESC LIMIT ?";
	         
	         
	         PreparedStatement ps = conn.prepareStatement(sql);
	         ps.setInt(1, count);
	         ResultSet rs = ps.executeQuery();

	         while (rs.next()) {
	        	 productsList.add(this.setProduct(rs));
	         }
	      } catch (Exception e) {
	         System.out.println("Can't get new product : " + e.getMessage());
	      } finally {
	         if (conn != null) {
	            try {
	               conn.close();
	            } catch (Exception e) {
	            }
	         }
	      }
		return productsList;
	}
	
	private Product setProduct(ResultSet rs){
		Product p = new Product();
		try{
    	 p.setId(rs.getInt("id"));
    	 p.setBrand(rs.getString("brand"));
    	 p.setTitle(rs.getString("title"));
    	 p.setSize(rs.getFloat("size"));
    	 p.setQuantity(rs.getInt("quantity"));
    	 p.setPrice(rs.getInt("price"));
    	 p.setImg(rs.getString("img"));
    	 p.setDescription(rs.getString("description"));
    	 p.setSpecifications(rs.getString("specifications"));
    	 p.setPromotion(rs.getString("promotion"));
    	 p.setStatus(rs.getInt("status"));
    	 p.setCreatedAt(rs.getDate("created_at"));
    	 p.setModifiedAt(rs.getDate("modified_at"));

		} catch (Exception e){
			
		}
		return p;
	}
	public List<Product> findViaInvoice(List<InvoiceDetail> invds){
		List<Product> productsList = new ArrayList<Product>();
		try { 
			 this.configDB();
			 for(InvoiceDetail d : invds ){
				 String sql = "SELECT * FROM product WHERE id = ? ";
		         PreparedStatement ps = conn.prepareStatement(sql);
		         ps.setInt(1, d.getProductId());
		         ResultSet rs = ps.executeQuery();
		         while (rs.next()) {
		        	 productsList.add(this.setProduct(rs));
		        	 System.out.println("\nYou just get product with id : " + d.getProductId());
		         }
			 }
			
	      } catch (Exception e) {
	         System.out.println("Can't get new product : " + e.getMessage());
	      } finally {
	         if (conn != null) {
	            try {
	               conn.close();
	            } catch (Exception e) {
	            }
	         }
	      }
		return productsList;
	}
	public List<Product> findFirst(int count){
		List<Product> productsList = new ArrayList<Product>();
		try { 
			 this.configDB();
	         String sql = "SELECT * FROM product WHERE status = 1 ORDER BY RAND() LIMIT ?";
	         
	         
	         PreparedStatement ps = conn.prepareStatement(sql);
	         ps.setInt(1, count);
	         ResultSet rs = ps.executeQuery();

	         while (rs.next()) {
	        	 productsList.add(this.setProduct(rs));
	         }
	      } catch (Exception e) {
	         System.out.println("Can't get new product : " + e.getMessage());
	      } finally {
	         if (conn != null) {
	            try {
	               conn.close();
	            } catch (Exception e) {
	            }
	         }
	      }
		return productsList;
	}
	public List<Product> findLowQuantity(int count){
		List<Product> productsList = new ArrayList<Product>();
		try { 
			 this.configDB();
	         String sql = "SELECT * FROM product WHERE status = 1 ORDER BY quantity ASC LIMIT ?";
	         
	         PreparedStatement ps = conn.prepareStatement(sql);
	         ps.setInt(1, count);
	         ResultSet rs = ps.executeQuery();

	         while (rs.next()) {
	        	 productsList.add(this.setProduct(rs));
	         }
	      } catch (Exception e) {
	         System.out.println("Can't get low quantity product : " + e.getMessage());
	      } finally {
	         if (conn != null) {
	            try {
	               conn.close();
	            } catch (Exception e) {
	            }
	         }
	      }
		return productsList;
	}
	public List<Product> findNew(int count){
		List<Product> productsList = new ArrayList<Product>();
		try { 
			 this.configDB();
	         String sql = "SELECT * FROM product ORDER BY id DESC LIMIT ?";
	         
	         PreparedStatement ps = conn.prepareStatement(sql);
	         ps.setInt(1, count);
	         ResultSet rs = ps.executeQuery();

	         while (rs.next()) {
	        	 productsList.add(this.setProduct(rs));
	         }
	      } catch (Exception e) {
	         System.out.println("Can't get new product : " + e.getMessage());
	      } finally {
	         if (conn != null) {
	            try {
	               conn.close();
	            } catch (Exception e) {
	            }
	         }
	      }
		return productsList;
	}
	
	public List<Product> findAll(){
		List<Product> productsList = new ArrayList<Product>();
		try { 
			 this.configDB();
	         String sql = "SELECT * FROM product";
	         PreparedStatement ps = conn.prepareStatement(sql);
	         ResultSet rs = ps.executeQuery();

	         while (rs.next()) {
	        	 productsList.add(this.setProduct(rs));
	         }
	      } catch (Exception e) {
	         System.out.println("Can't get product by id : " + e.getMessage());
	      } finally {
	         if (conn != null) {
	            try {
	               conn.close();
	            } catch (Exception e) {
	            }
	         }
	      }
		return productsList;
	}
	public Product findById(int id) {
		try { 
			 this.configDB();
	         String sql = "SELECT * FROM product WHERE id = ? AND status = 1";
	         PreparedStatement ps = conn.prepareStatement(sql);
	         ps.setInt(1, id);
	         ResultSet rs = ps.executeQuery();

	         while (rs.next()) {
	        	 return this.setProduct(rs);
	         }
	      } catch (Exception e) {
	         System.out.println("Can't get product by id : " + e.getMessage());
	      } finally {
	         if (conn != null) {
	            try {
	               conn.close();
	            } catch (Exception e) {
	            }
	         }
	      }
		return new Product(0, " ", " ", 0, 0, 0, " ", " ");
	}
	public List<Product> sortAllByTitleAsc(){
		List<Product> productsList = new ArrayList<Product>();
		try { 
			 this.configDB();
	         String sql = "SELECT * FROM product WHERE ORDER BY title ASC";
	         PreparedStatement ps = conn.prepareStatement(sql);
	         ResultSet rs = ps.executeQuery();

	         while (rs.next()) {
	        	 productsList.add(this.setProduct(rs));
	         }
	      } catch (Exception e) {
	         System.out.println("Can't get product by brand : " + e.getMessage());
	      } finally {
	         if (conn != null) {
	            try {
	               conn.close();
	            } catch (Exception e) {
	            }
	         }
	      }
		return productsList;
	}
	public List<Product> sortAllByTitleDesc(){
		List<Product> productsList = new ArrayList<Product>();
		try { 
			 this.configDB();
	         String sql = "SELECT * FROM product WHERE ORDER BY title Desc";
	         PreparedStatement ps = conn.prepareStatement(sql);
	         ResultSet rs = ps.executeQuery();

	         while (rs.next()) {
	        	 productsList.add(this.setProduct(rs));
	         }
	      } catch (Exception e) {
	         System.out.println("Can't get product by brand : " + e.getMessage());
	      } finally {
	         if (conn != null) {
	            try {
	               conn.close();
	            } catch (Exception e) {
	            }
	         }
	      }
		return productsList;
	}
	public List<Product> findByBrand(String brand){
		
		List<Product> productsList = new ArrayList<Product>();
		try { 
			 this.configDB();
	         String sql = "SELECT * FROM product WHERE brand = ? AND status = 1";
	         PreparedStatement ps = conn.prepareStatement(sql);
	         ps.setString(1, brand); 
	         ResultSet rs = ps.executeQuery();

	         while (rs.next()) {
	        	 productsList.add(this.setProduct(rs));
	         }
	      } catch (Exception e) {
	         System.out.println("Can't get product by brand : " + e.getMessage());
	      } finally {
	         if (conn != null) {
	            try {
	               conn.close();
	            } catch (Exception e) {
	            }
	         }
	      }
		return productsList;
	}
	public List<Product> findByBrandWithPriceAsc(String brand){
		List<Product> productsList = new ArrayList<Product>();
		try { 
			 this.configDB();
	         String sql = "SELECT * FROM product WHERE brand = ? AND status = 1 ORDER BY id ASC";
	         PreparedStatement ps = conn.prepareStatement(sql);
	         ps.setString(1, brand); 
	         ResultSet rs = ps.executeQuery();

	         while (rs.next()) {
	        	 productsList.add(this.setProduct(rs));
	         }
	      } catch (Exception e) {
	         System.out.println("Can't get product by brand : " + e.getMessage());
	      } finally {
	         if (conn != null) {
	            try {
	               conn.close();
	            } catch (Exception e) {
	            }
	         }
	      }
		return productsList;
	}
	public List<Product> findNewByBrand(String brand, int count){
		List<Product> productsList = new ArrayList<Product>();
		
		try { 
			 this.configDB();
	         String sql = "SELECT * FROM product WHERE brand = ? AND status = 1 ORDER BY id DESC LIMIT ?";
	         System.out.println(sql);
	         PreparedStatement ps = conn.prepareStatement(sql);
	         ps.setString(1, brand);
	         ps.setInt(2, count);
	         ResultSet rs = ps.executeQuery();

	         while (rs.next()) {
	        	 productsList.add(this.setProduct(rs));
	         }
	      } catch (Exception e) {
	         System.out.println("Can't get new product : " + e.getMessage());
	      } finally {
	         if (conn != null) {
	            try {
	               conn.close();
	            } catch (Exception e) {
	            }
	         }
	      }
		return productsList;
	}
	
}
