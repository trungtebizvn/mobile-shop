package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ModelBase {
	final String ERROR = "error";
	final String SUCCESS = "success";
	protected Connection conn;

	public String getDateToday() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		return formatter.format(new Date());
	}

	public Date convertStringToDate(String date){
		Date result = new Date();
	
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			
			result =  (Date)formatter.parse(date);
		}  catch (ParseException e) {
			e.printStackTrace();
		}
		return result;
	}

	public void configDB() {
		this.conn = null;
		try {
			String URL = "jdbc:mysql://localhost/shopping";
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(URL, "root", "root");
		} catch (Exception e) {
		}
	}
}
