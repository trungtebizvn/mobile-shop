package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import entities.Customer;

public class CustomerModel extends ModelBase{

	public Customer findById(int cid){

		try { 
			 this.configDB();
	         String sql = "SELECT * FROM customer WHERE id = ?";
	         PreparedStatement ps = conn.prepareStatement(sql);
	         ps.setInt(1, cid);
	         ResultSet rs = ps.executeQuery();

	         while (rs.next()) {
	        	 Customer c = new Customer();
	        	 c.setId(rs.getInt("id"));
	        	 c.setName(rs.getString("name"));
	        	 c.setPhone(rs.getString("phone"));
	        	 c.setEmail(rs.getString("email"));
	        	 c.setAddress(rs.getString("address"));
	        	 c.setCreatedAt(rs.getDate("created_at"));
	        	 return c;
	         }
	      } catch (Exception e) {
	         System.out.println("Can't get all invoice detail by id of invoice : " + e.getMessage());
	      } finally {
	         if (conn != null) {
	            try {
	               conn.close();
	            } catch (Exception e) {
	            }
	         }
	      }
		return new Customer(0,"jack","0","jack","jack");
	}
	public int insert(Customer c){
		this.configDB();
		int customer_id = 0;
		try { 
			 this.configDB();
	         String sql = "INSERT INTO customer values(NULL,?,?,?,?,?)";
	         PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
	         ps.setString(1, c.getName());
	         ps.setString(2, c.getPhone());
	         ps.setString(3, c.getEmail());
	         ps.setString(4, c.getAddress());
	         ps.setString(5, this.getDateToday()) ;
	         if(ps.executeUpdate() > 0){
	        	//Get id of customer just insert
	        	 ResultSet rs = ps.getGeneratedKeys();
		         if(rs.next())
	             {
		        	 customer_id =  rs.getInt(1); 
	                 System.out.println("Customer id just insert :  " + customer_id);
	             }
	         }


	      } catch (Exception e) {
	         System.out.println("Insert customer is failed. Because  " + e.getMessage());
	      } finally {
	         if (this.conn != null) {
	            try {
	               this.conn.close();
	            } catch (Exception e) {
	            }
	         }
	      }
		return customer_id;
	}
}
