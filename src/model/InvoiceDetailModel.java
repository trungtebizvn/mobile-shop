package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.opensymphony.xwork2.ActionContext;

import entities.Cart;
import entities.Invoice;
import entities.InvoiceDetail;
import entities.Product;

public class InvoiceDetailModel extends ModelBase {
	private Map<Product, Integer> cart = new HashMap<Product, Integer>();
	private ProductModel pm = new ProductModel();
	
	public Set<Product> getProducts() {
		Cart cartEntity = (Cart) ActionContext.getContext().getSession()
				.get("cart");
		cart = cartEntity.getProducts();
		return cart.keySet();
	}

	public List<InvoiceDetail> findByInvoiceId(int invoice_id){
		List<InvoiceDetail> inv_details = new ArrayList<InvoiceDetail>();
		try { 
			 this.configDB();
	         String sql = "SELECT * FROM invoice_detail WHERE invoice_id = ?";
	         PreparedStatement ps = conn.prepareStatement(sql);
	         ps.setInt(1, invoice_id);
	         ResultSet rs = ps.executeQuery();

	         while (rs.next()) {
	        	 InvoiceDetail p = new InvoiceDetail();
	        	 p.setId(rs.getInt("id"));
	        	 p.setInvoiceId(rs.getInt("invoice_id"));
	        	 p.setProductId(rs.getInt("product_id"));
	        	 p.setQuantity(rs.getInt("quantity"));
	        	 p.setAmount(rs.getDouble("amount"));
	        	 p.setStatus(rs.getString("status"));
	        	 p.setCreatedAt(rs.getDate("created_at"));
	        	 inv_details.add(p);
	        	 System.out.println("Detail : " + p.getId());
	         }
	      } catch (Exception e) {
	         System.out.println("Can't get all invoice detail by id of invoice : " + e.getMessage());
	      } finally {
	         if (conn != null) {
	            try {
	               conn.close();
	            } catch (Exception e) {
	            }
	         }
	      }
		return inv_details;
	}
	public boolean insert(int invoice_id) {
		this.configDB();
		Boolean ret = false;
		try {
			this.configDB();

			Set<Product> keys = this.getProducts();
			for (Product product : keys) {
				
				String sql = "INSERT INTO invoice_detail values(NULL,?,?,?,?,?,?)";
				PreparedStatement ps = conn.prepareStatement(sql,
						Statement.RETURN_GENERATED_KEYS);
				int quantity = cart.get(product);
				int pid = product.getId();
				double amount = quantity * product.getPrice();
				
				ps.setInt(1, invoice_id);
				ps.setInt(2, pid);
				ps.setInt(3, quantity);
				ps.setDouble(4, amount);
				ps.setString(5, (new Invoice()).STATUS_PENDING);
				ps.setString(6, this.getDateToday());
				if( ps.executeUpdate() > 0 )
					this.pm.subtractQuantity(product, quantity);
					System.out.println("Insert invoice detail " + product.getTitle() + ", quantity " + quantity);
			}
	

		} catch (Exception e) {
			System.out.println("Insert invoice is failed. Because  "
					+ e.getMessage());
		} finally {
			if (this.conn != null) {
				try {
					this.conn.close();
				} catch (Exception e) {
				}
			}
		}
		return ret;
	}
}
