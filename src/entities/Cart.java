package entities;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import model.ProductModel;

public class Cart {

    private final Map<Product, Integer> cart = new HashMap<Product, Integer>();
    private ProductModel pm = new ProductModel();

    public Cart() {
    	
    }
    
    public void addProduct(Product p) {
        Integer amount = cart.get(p);
        int qty = this.pm.getQuantity(p.getId());
        System.out.println("Product just added which was existed in cart : " + cart.containsKey(p) + " - " + amount);
        cart.put(p, amount == null ? (qty > 0 ? 1 : 0) : (amount == qty ? amount : amount + 1));
        
    }
    public void reduceProduct(Product p){
    	Integer amount = cart.get(p);
    	cart.put(p, amount == 1 ? 1 : (amount -1 ));
    }
    public boolean removeProduct(Product p) {
    	if (cart.containsKey(p)){
    		cart.remove(p);
    		return true;
    	}
    	return false;
    }

    public Map<Product, Integer> getProducts() {
        return cart;
    }

    public double getTotalPrice() {
    	double total = 0;

        Set<Product> keys = cart.keySet();
        for (Product product : keys) {
            total += product.getPrice() * cart.get(product);
        }
        
        return total;
    }
    public int getAmount(){
    	int amount = 0;
    	Set<Product> keys = cart.keySet();
        for (Product product : keys) {
            amount +=  cart.get(product);
        }
        return amount;
    }
}