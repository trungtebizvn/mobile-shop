package entities;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="product")
public class Product implements Serializable {

	private Integer id;
	private String brand;
	private String title;
	private float size;
	private int quantity;
	private double price;
	private String img;
	private String description;
	private String specifications;
	private String promotion;
	private int status;
	private Date createdAt;
	private Date modifiedAt;

	@Id
	@GeneratedValue
	@Column(name="id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="brand")
	public String getBrand() {
		return brand;
	}
	
	public void setBrand(String brand) {
		this.brand = brand;
	}
	@Column(name="title")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	@Column(name="size")
	public float getSize() {
		return size;
	}

	public void setSize(float size) {
		this.size = size;
	}
	@Column(name="quantity")
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	@Column(name="price")
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	@Column(name="img")
	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}
	@Column(name="description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	@Column(name="promotion")
	public String getPromotion() {
		return promotion;
	}

	public void setPromotion(String promotion) {
		this.promotion = promotion;
	}
	@Column(name="status")
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	@Column(name="created_at")
	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	@Column(name="modified_at")
	public Date getModifiedAt() {
		return modifiedAt;
	}

	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public Product() {
	}

	public Product(int id, String brand, String title, float size, int quantity,
			double price, String img, String description) {
		this.id = id;
		this.brand = brand;
		this.title = title;
		this.size = size;
		this.quantity = quantity;
		this.price = price;
		this.img = img;
		this.description = description;
	}

	public Product(String brand, String title, float size, int quantity,
			double price, String img, String description, String promotion,
			int status, Date createdAt, Date modifiedAt) {
		this.brand = brand;
		this.title = title;
		this.size = size;
		this.quantity = quantity;
		this.price = price;
		this.img = img;
		this.description = description;
		this.promotion = promotion;
		this.status = status;
		this.createdAt = createdAt;
		this.modifiedAt = modifiedAt;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj == null || obj.getClass() != this.getClass()) {
			return false;
		}
		Product p = (Product) obj;
		return id == p.id;
	}
	
	@Override
	public int hashCode(){
		final int prime = 29;
		int result = 1;
		result = prime * result + ((brand == null) ? 0 : brand.hashCode()) + id;
		return result;
	}

	public String getSpecifications() {
		return specifications;
	}

	public void setSpecifications(String specifications) {
		this.specifications = specifications;
	}

	

}
