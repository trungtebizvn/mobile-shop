package services;

import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import model.CustomerModel;
import model.InvoiceModel;

import com.opensymphony.xwork2.ActionContext;

import entities.Cart;
import entities.Invoice;
import entities.Product;

public class Emailer{

   private String from;
   private String password;
   private String to;
   private String subject;
   private String body;

   static Properties properties = new Properties();
   static
   {
      properties.put("mail.smtp.host", "smtp.gmail.com");
      properties.put("mail.smtp.socketFactory.port", "465");
      properties.put("mail.smtp.socketFactory.class",
                     "javax.net.ssl.SSLSocketFactory");
      properties.put("mail.smtp.auth", "true");
      properties.put("mail.smtp.port", "465");
   }

   public void initialize(){
	   this.from = "java.pascal2@gmail.com";
	   this.password = "ambulane";
	   this.subject = "Mobile supermarket : Place order is successful";
	   
	   /* Set body */
	   Cart cart = (Cart) ActionContext.getContext().getSession().get("cart");
	   Map<Product, Integer> products = cart.getProducts();
	   
	   InvoiceModel im = new InvoiceModel();
	   Invoice invoice = im.findById((Integer)ActionContext.getContext().getSession().get("invoice_id"));
	   
	   double total = 0;
	   
	   String content;
	   content ="<div id='your-order'>";
	   Set<Product> keys = products.keySet();
       for (Product product : keys) {
    	   double amount = product.getPrice() * products.get(product);
           total += amount;
           content +="<span>Thank you for your online order. We have confirmed your order as follow :</span><br>";
    	   content +="<span>Order confirmation No. : GTS115112" + invoice.getId() + "</span>";
    	   content +="<div>Product name : " + product.getTitle() + "<br>";
    	   content +="Qty : " + products.get(product) +"<br>";
    	   content +="Amount : $" + amount + "</div><p>";
       }
       content +="<div>Total : $" + total +"</div>";
       content +="</div>";
       
       this.body = content;
       
       /* Get email of the customer */
       CustomerModel cm = new CustomerModel();
       this.to = cm.findById(invoice.getCustomerId()).getEmail();
   }

   public void send() 
   {
	  this.initialize();

      try
      {
         Session session = Session.getDefaultInstance(properties,  
            new javax.mail.Authenticator() {
            protected PasswordAuthentication 
            getPasswordAuthentication() {
            return new 
            PasswordAuthentication(from, password);
            }});
         
         
         Message message = new MimeMessage(session);
         String htmlText = body.replaceAll("\n","<br>");
         message.setContent(htmlText, "text/html");
         message.setFrom(new InternetAddress(this.from));
         message.setRecipients(Message.RecipientType.TO, 
            InternetAddress.parse(this.to));
         message.setSubject(this.subject);
         message.setText(this.body);
         Transport.send(message);
      }
      catch(Exception e)
      {
         e.printStackTrace();
      }
   }


   public static Properties getProperties() {
      return properties;
   }

   public static void setProperties(Properties properties) {
      Emailer.properties = properties;
   }
}
